## Industrie 4.0 Maintenance Management

### Overview

This project is a web application designed to manage industrial machinery maintenance schedules and tasks. It provides functionalities to add and manage machines, schedule maintenance tasks, and track maintenance history. The application is built with a React frontend and a Node.js backend.

### Features

- **Machine Management**: Add and manage industrial machines with details like name and image.
- **Maintenance Scheduling**: Schedule maintenance tasks for machines, including setting intervals and dates.
- **Task Tracking**: View and track maintenance tasks, including marking tasks as completed.
- **Calendar View**: View maintenance tasks on a calendar, with indicators for scheduled tasks.
- **User Management**: Login functionality to manage and track tasks per user.

### Technologies Used

- **Frontend**: React, TypeScript, Bootstrap, React Calendar
- **Backend**: Node.js, Express.js, Multer, Nodemailer
- **Database**: JSON files (or alternatively, a database like MongoDB)
- **Icons**: Material UI Icons

### Getting Started

#### Prerequisites

- Node.js
- npm

#### Installation

1. Clone the repository:
   ```sh
   git clone https://git.thm.de/rmlb37/industrie4.0.git
   cd industrie4.0
   ```

2. Install dependencies for both frontend and backend:
   ```sh
   npm install
   ```

#### Running the Application

1. Start the backend server:
   ```sh
   npm run start:api
   ```

2. Start the frontend:
   ```sh
   npm run dev
   ```

3. Open your browser and navigate to `http://localhost:5173` to view the application.

4. To add a maintenance and mashine, you have to register and log in. Users have sign in with a real e-mail if they
want to assign them to a maintenance.

---
### Folder Structure

- `src/`: Contains the React frontend code.
- `server.mjs/`: Contains the Node.js backend code.
- `models/`: Contains the data models for the application.
- `services/`: Contains services for data fetching and manipulation.
- `assets/`: Contains static assets like images.
- `uploads/`: Saves all Imgeas and Pdf

### API Endpoints

- **Machines**
    - `GET /api/maschinen`: Get all machines
    - `POST /api/maschinen`: Add a new machine

- **Maintenance**
    - `GET /api/wartungen`: Get all maintenance tasks
    - `POST /api/wartungen`: Add a new maintenance task
    - `PUT /api/wartungen/:id`: Update a maintenance task

- **Users**
    - `GET /api/users`: Get all users
    - `POST /api/users`: Add a new user

    

### Contact

For any questions or issues, please contact 
- [rico.marco.libesch@mni.thm.de](mailto:rico.marco.libesch@mni.thm.de) Matrikelnummer: 5324095
- [martin.holstein@mni.thm.de](mailto:martin.holstein@mni.thm.de) Matrikelnummer: 5276677
- [anhelina.nazarchuk@mni.thm.de](mailto:anhelina.nazarchuk@mni.thm.de) Matrikelnummer: 5552281
- [rodrigue.fokam.fodouop@mni.thm.de](mailto:rodrigue.fokam.fodouop@mni.thm.de) Matrikelnummer: 5444030

---

