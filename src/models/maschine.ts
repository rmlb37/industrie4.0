/**
 * Maschine Class
 * Represents a machine with its properties.
 */
export default class Maschine {
    id: number;          // Unique identifier for the machine
    name: string;        // Name of the machine
    created: string;     // Creation date of the machine
    picture: string;     // Path to the picture of the machine
    zustaendige?: string; // Optional property for the person responsible for the machine

    /**
     * Constructor to initialize a Maschine object.
     * @param id - Unique identifier for the machine.
     * @param name - Name of the machine.
     * @param picture - Path to the picture of the machine.
     * @param created - Creation date of the machine.
     * @param zustaendige - Person responsible for the machine.
     */
    constructor(
        id: number,
        name: string,
        picture: string,
        created: string,
        zustaendige: string,
    ) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.created = created;
        this.zustaendige = zustaendige;
    }
}
