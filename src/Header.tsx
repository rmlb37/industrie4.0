import React from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import logo from './assets/img.png';
import { IconButton } from "@mui/material";
import { AccountCircle } from "@mui/icons-material";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";

/**
 * Header Component
 * This component renders the header of the application.
 * It includes navigation links, a logo, and a user account button.
 */
const Header: React.FC = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const username = sessionStorage.getItem('username');

    // Object to map routes to their respective page names
    const pageNames: { [key: string]: string } = {
        '/overview': 'Maintenance Overview',
        '/calendar': 'Calendar',
        '/history': 'Machine Overview',
        '/add-machine': 'Add Machine',
        '/add-maintenance': 'Add Maintenance',
        '/login': 'User Login',
    };

    // Handle login/logout functionality
    const handleLoginClick = () => {
        if (username) {
            // Logout functionality
            sessionStorage.removeItem('username');
            navigate('/login');
        } else {
            // Login functionality
            navigate('/login');
        }
    };

    return (
        <div className="pos-f-t">
            <Navbar fixed="top" bg="light" expand="lg" className="py-0">
                <Container fluid className="d-flex justify-content-between align-items-center shadow-sm px-3">
                    <Navbar.Brand href="#" className="d-flex flex-column align-items-center">
                        <img src={logo} alt="Logo" width="250" height="64" style={{ width: 'auto', height: 64 }} />
                        <div style={{ marginTop: '18px', fontSize: '25px' }}>
                            {pageNames[location.pathname] || 'Home'}
                        </div>
                    </Navbar.Brand>
                    <Nav className="me-auto mx-5">
                        <Nav.Link as={Link} to="/overview">Maintenances</Nav.Link>
                        <Nav.Link as={Link} to="/calendar">Calendar</Nav.Link>
                        <Nav.Link as={Link} to="/history">History</Nav.Link>
                        <NavDropdown title="Add" id="basic-nav-dropdown" className={!username ? 'd-none' : ''}>
                            <NavDropdown.Item as={Link} to="/add-machine">Machine</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/add-maintenance">Maintenance</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <div className="ml-auto d-flex align-items-center" style={{ marginRight: '20px' }}>
                        {username && <span className="mr-2">{username}</span>}
                        <IconButton onClick={handleLoginClick}>
                            <AccountCircle className="!fill-secondary hover:!fill-primary !cursor-pointer" />
                        </IconButton>
                    </div>
                </Container>
            </Navbar>
        </div>
    );
}

export default Header;
