/**
 * Register page
 *
 * This component provides a user interface for registering a new user.
 * It includes form fields for the username, email, password, and password confirmation,
 * and communicates with the backend to create a new user. Upon successful registration,
 * the user is redirected to the login page.
 *
 */


import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Container, Row, Col, Alert } from 'react-bootstrap';
import { TextField, InputAdornment } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import LockIcon from '@mui/icons-material/Lock';
import EmailIcon from '@mui/icons-material/Email';
import axios from 'axios';
import '../styles/globals.css';

const Register: React.FC = () => {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const navigate = useNavigate();

    const handleRegister = async (event: React.FormEvent) => {
        event.preventDefault();

        // Check if passwords match
        if (password !== confirmPassword) {
            setErrorMessage('Passwords dont match');
            return;
        }

        try {
            const response = await axios.post('http://localhost:3002/api/users', {
                username,
                email,
                password,
            });

            if (response.data.success) {
                navigate('/login');
            } else {
                setErrorMessage(response.data.message || 'registration failed');
            }
        } catch (error) {
            setErrorMessage('registration failed');
        }
    };

    return (
        <Container className="mt-0 main-content">
            <Row className="mb-4">
                <Col md={12}>
                    <h2 className="text-left">Registration</h2>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col md={6} className="d-flex flex-column align-items-center">
                    <form noValidate autoComplete="off" className="w-100" onSubmit={handleRegister}>
                        <TextField
                            label="username"
                            variant="outlined"
                            fullWidth
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircleIcon />
                                    </InputAdornment>
                                ),
                            }}
                            className="mb-3"
                        />
                        <TextField
                            label="email"
                            variant="outlined"
                            fullWidth
                            type="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <EmailIcon />
                                    </InputAdornment>
                                ),
                            }}
                            className="mb-3"
                        />
                        <TextField
                            label="password"
                            variant="outlined"
                            fullWidth
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <LockIcon />
                                    </InputAdornment>
                                ),
                            }}
                            className="mb-3"
                        />
                        <TextField
                            label="repeat password"
                            variant="outlined"
                            fullWidth
                            type="password"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <LockIcon />
                                    </InputAdornment>
                                ),
                            }}
                            className="mb-3"
                        />
                        {errorMessage && (
                            <Alert variant="danger" className="mt-2">
                                {errorMessage}
                            </Alert>
                        )}
                        <div className="d-flex justify-content-center">
                            <Button
                                type="submit"
                                className="mt-3"
                                style={{ backgroundColor: '#2E5A66', borderColor: '#4A5C66', color: 'white' }}
                            >
                                Register
                            </Button>
                        </div>
                    </form>
                </Col>
            </Row>
        </Container>
    );
};

export default Register;
