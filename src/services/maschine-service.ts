// This class provides services for managing and retrieving information about machines and their maintenance activities.
import Maschine from "../models/maschine";
import Wartung from "../models/wartung";

export default class MaschineService{
  // Retrieves a machine by its ID from the API
    static getMaschine(id: number|string): Promise<Maschine|null> {
        return fetch(`http://localhost:3002/api/maschinen/${id}`)
          .then(response => response.json())
          .then(data => this.isEmpty(data) ? null : data)
          .catch(error => this.handleError(error));
        }

       // Checks if an object is empty
      static isEmpty(data: Object): boolean {
        return Object.keys(data).length === 0;
      }
    
      // Handles errors by logging them to the console

      static handleError(error: Error): void {
        console.error(error);
      }

      // Filters and returns machines that have no pending maintenance
      static getMachinesWithNoPendingMaintenance(maschinen: Maschine[], wartungen: Wartung[]){
        const filteredmaschinen = maschinen.filter(machine => {
          const machineWartungen = wartungen.filter(wartung => wartung.maschineId == machine.id);
          return machineWartungen.length === 0 || machineWartungen.every(wartung => wartung.isCompleted);
      });
      return filteredmaschinen;
      }


      // Filters and returns machines that have pending maintenance
      static getMachinesWithPendingMaintenance(maschinen: Maschine[], wartungen: Wartung[]){
        const filteredmaschinen = maschinen.filter(machine => {
          const machineWartungen = wartungen.filter(wartung => wartung.maschineId == machine.id);
          return machineWartungen.length > 0 && machineWartungen.find(wartung => !wartung.isCompleted);
      });
      return filteredmaschinen;
    }
    
    // Checks if a machine has any pending maintenance tasks
    static checkDirectory(machine: Maschine|null, wartungen: Wartung[]):boolean{
      
      if(machine)  {
        const machineWartungen = wartungen.filter(wartung => wartung.maschineId == machine.id)
        if(machineWartungen.length > 0 && machineWartungen.find(wartung => !wartung.isCompleted)) 
          return true;
      }
        return false;
    }

  // Retrieves the list of completed maintenance tasks for a given machine
  static getMaintenanceList(machine: Maschine|null, wartungen: Wartung[]){
      if(machine){
        const machineWartungen = wartungen.filter(wartung => wartung.maschineId == machine.id && wartung.isCompleted)
        return machineWartungen;
      }
      return wartungen;
    }

    
  }
  
  
