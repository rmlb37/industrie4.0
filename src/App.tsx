// App.tsx
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from './Header';
import MachineList from './components/MachineList';
import AddMachine from './pages/AddMachine';
import AddMaintenance from './pages/AddMaintenance';
import Calendar from './pages/Calendar';
import History from './pages/History';
import Login from './pages/Login';
import Register from './pages/Register';
import './App.css';
import {FunctionComponent} from "react";
import MaschineDetail from './pages/MaschineDetail';
import MaschineEdit from './pages/MaschineEdit';
import EditMaintenance from './pages/EditMaintenance';
import {Container} from "react-bootstrap";
import ShowMaintenance from './pages/ShowMaintenanceList';

const App: FunctionComponent = () => {
  return (
    <Router>
      <Header />
      <Container className="main-content">
        <Routes>
          <Route path="/" element={<MachineList />} />
          <Route path="/overview" element={<MachineList />} />
          <Route path="/add-machine" element={<AddMachine />} />
          <Route path="/add-maintenance" element={<AddMaintenance />} />
          <Route path="/calendar" element={<Calendar />} />
          <Route path="/history" element={<History />} />
          <Route path="/overview/edit/:id" element={<MaschineEdit />} />
          <Route path="/overview/editMaintenance/:id" element={<EditMaintenance />} />
          <Route path="/overview/maintenanceList/:id" element={<ShowMaintenance />} />
          <Route path="/overview/:id" element={<MaschineDetail />} />
          <Route path="/Login" element={<Login />} />
            <Route path="/register" element={<Register />} />
        </Routes>
      </Container>
      <div>
        
      </div>
    </Router>
  );
};

export default App;

