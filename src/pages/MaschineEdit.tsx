/**  This component, MaschineEdit, serves as the interface for editing machine details based on the provided ID in the route parameters.
* It utilizes React's FunctionComponent along with hooks such as useState and useEffect for managing state and side effects.
* The component imports necessary dependencies including useParams from react-router-dom for accessing route parameters,
* Maschine from '../models/maschine' for type definitions, and MaschineService from '../services/maschine-service' for fetching machine data.
*/



// Import necessary dependencies from React and react-router-dom
import  { FunctionComponent, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

// Import Maschine model and MaschineForm component
import Maschine from '../models/maschine';
import MaschineService from '../services/maschine-service';
import MaschineForm from './MaschineForm';

// Define the type for route parameters
type Params = { id: string };
  
// Define MaschineEdit as a FunctionComponent
const MaschineEdit: FunctionComponent = () => {

  // Retrieve the 'id' parameter from the URL using useParams
  const { id } = useParams<Params>();

  // State to hold the machine details, initialized as null initially
  const [maschine, setMaschine] = useState<Maschine|null>(null);
  
  // useEffect hook to fetch machine data when component mounts
  useEffect(() => {
        if (id){

          // Fetch machine details from MaschineService using 'id'
          MaschineService.getMaschine(id).then(machine => setMaschine(machine));
        }

  }, []);

  return (
    <div>
      {maschine ? (
        <div className="row main-content">
          <h2 className="header center"> {maschine.name} editieren</h2>
          <MaschineForm maschine={maschine} ></MaschineForm>
        </div>
      ) : (
        <h4 className="center">Keine Maschine zu darstellen !</h4>
      )}
    </div>
  );
};
  
// Export MaschineEdit component as default
export default MaschineEdit;