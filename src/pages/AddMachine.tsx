/**
 * AddMachine Component
 * This component is used to add a new machine.
 * It provides a form where the user can enter a machine name and upload an image.
 * After input, the data is validated and sent to the server to save the machine in the database.
 */

import React, { useState } from 'react';
import { Button, Container, Row, Col, Alert, Image } from 'react-bootstrap';
import { TextField, InputAdornment } from '@mui/material';
import PrecisionManufacturingIcon from '@mui/icons-material/PrecisionManufacturing';
import axios from 'axios';
import '../styles/globals.css';
import defaultImage from '../assets/defaultImage.png';

const AddMachine: React.FC = () => {
    // State variables to manage form inputs and messages
    const [machineName, setMachineName] = useState('');
    const [machineImage, setMachineImage] = useState<File | null>(null);
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');
    const [uploadedImageUrl, setUploadedImageUrl] = useState('');

    // Handler for changes in the machine name input field
    const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setMachineName(e.target.value);
    };

    // Handler for changes in the image input field
    const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files && e.target.files[0];
        if (file) {
            const fileType = file.type;
            if (fileType === 'image/jpeg' || fileType === 'image/png') {
                setMachineImage(file);
                setErrorMessage(''); // Clear previous error messages
            } else {
                setErrorMessage('Only .jpg and .png files are allowed.');
            }
        }
    };

    // Handler for the save function
    const handleSave = async () => {
        // Check if the machine name is provided
        if (!machineName) {
            setErrorMessage('Please provide a machine name.');
            return;
        }

        try {
            // Check if a machine with the same name already exists
            const existingMachinesResponse = await axios.get('http://localhost:3002/api/maschinen');
            const existingMachines = existingMachinesResponse.data;
            const duplicateMachine = existingMachines.find((machine: any) => machine.name === machineName);

            if (duplicateMachine) {
                setErrorMessage('A machine with this name already exists.');
                return;
            }

            let imageUrl = '';

            if (machineImage) {
                // Upload image
                const formData = new FormData();
                formData.append('file', machineImage);

                const uploadResponse = await axios.post('http://localhost:3002/upload', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                });

                if (uploadResponse.status !== 200) {
                    throw new Error('Image upload failed');
                }

                const uploadData = uploadResponse.data;
                imageUrl = uploadData.fileUrl;

                // Clear the image immediately after a successful upload
                setMachineImage(null);
                setUploadedImageUrl(uploadData.fileUrl);
            } else {
                // Use default image if no image is uploaded
                imageUrl = defaultImage;
            }

            // Create a new machine object
            const machineData = {
                id: Date.now(),
                name: machineName,
                picture: imageUrl,
                created: new Date().toISOString().split('T')[0],
                zustaendige: ''  // responsible person
            };

            // Send the new machine to the backend server
            const response = await axios.post('http://localhost:3002/api/maschinen', machineData, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            // Handle server response
            if (response.status === 201 || response.status === 200) {
                setSuccessMessage('Machine added successfully.');
                setMachineName('');
                setErrorMessage('');
                setTimeout(() => {
                    setSuccessMessage('');
                    setUploadedImageUrl('');
                }, 3000); // Clear success message after 3 seconds
            } else {
                setErrorMessage('Failed to add machine.');
            }
        } catch (error) {
            console.error('Error:', error);
            setErrorMessage('An error occurred. Please try again.');
        }
    };

    return (
        <Container className="mt-0 main-content">
            <Row className="mb-4">
                <Col md={12}>
                    <h2 className="text-left">Enter a name for the machine:</h2>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col md={6} className="d-flex flex-column align-items-center">
                    <form noValidate autoComplete="off" className="w-100">
                        <TextField
                            label="Machine Name"
                            variant="outlined"
                            fullWidth
                            value={machineName}
                            onChange={handleNameChange}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <PrecisionManufacturingIcon />
                                    </InputAdornment>
                                ),
                            }}
                        />
                        <div className="mt-3 mb-4">
                            <label htmlFor="machineImage" className="form-label">
                                Upload Image
                            </label>
                            <input
                                type="file"
                                accept=".jpg,.png"
                                className="form-control"
                                id="machineImage"
                                onChange={handleImageChange}
                            />
                            {/* Display error message */}
                            {errorMessage && (
                                <Alert variant="danger" className="mt-2">
                                    {errorMessage}
                                </Alert>
                            )}
                            {/* Display success message */}
                            {successMessage && (
                                <Alert variant="success" className="mt-2">
                                    {successMessage}
                                </Alert>
                            )}
                            {/* Display image preview if an image is selected */}
                            {machineImage && !successMessage && (
                                <div className="mt-3">
                                    <Image src={URL.createObjectURL(machineImage)} thumbnail />
                                </div>
                            )}
                            {/* Display uploaded image URL */}
                            {uploadedImageUrl && (
                                <div className="mt-3">
                                    <Image src={`http://localhost:3002${uploadedImageUrl}`} thumbnail />
                                </div>
                            )}
                        </div>
                        <div className="d-flex justify-content-center">
                            <Button
                                className="mt-3"
                                onClick={handleSave}
                                style={{ backgroundColor: '#2E5A66', borderColor: '#4A5C66', color: 'white' }}
                            >
                                Save
                            </Button>
                        </div>
                    </form>
                </Col>
            </Row>
        </Container>
    );
};

export default AddMachine;
