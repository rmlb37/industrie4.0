import React, { Component } from 'react';
import { Row, Col, Container, Card, ListGroup,Modal, Button } from 'react-bootstrap';
import Calendar, { CalendarProps } from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { Link } from 'react-router-dom';
import { CalendarMonth, Construction } from '@mui/icons-material';
import Maschine from '../models/maschine';
import Wartung from '../models/wartung';
import axios from 'axios';
import '../styles/calendar.css';
import '../styles/globals.css';


/* SidebarMenu Component
 * This component renders the sidebar menu with links to other pages.
 * The "Neue Wartung" button is only displayed if the user is logged in.
 */
interface SidebarMenuProps {
    resetFilter: () => void;
}

const SidebarMenu: React.FC<SidebarMenuProps> = ({ resetFilter }) => {
    const username = sessionStorage.getItem('username');

    return (
        <Row className="sidebar-menu">
            <Col>
                <div className="menu-item">
                    <Link to="/calendar" className="menu-link" onClick={resetFilter}>
                        <CalendarMonth className="menu-icon" />
                        <span>Calender</span>
                    </Link>
                </div>
                {username && (
                    <div className="menu-item">
                        <Link to="/add-maintenance" className="menu-link">
                            <Construction className="menu-icon" />
                            <span>New Maintenance</span>
                        </Link>
                    </div>
                )}
            </Col>
        </Row>
    );
};

/* CalendarPage State Interface
 * This interface defines the shape of the state for the CalendarPage component.
 */

interface CalendarPageState {
    selectedMachine: string;
    machines: Maschine[];
    selectedDate: Date;
    tasksForSelectedDate: Wartung[];
    showModal: boolean;
    currentTask: Wartung | null;
}

/* CalendarPage Component
 * This component represents the calendar page where users can view and manage maintenance tasks.
 */

class CalendarPage extends Component<{}, CalendarPageState> {
    state: CalendarPageState = {
        selectedMachine: '',
        machines: [],
        selectedDate: new Date(),
        tasksForSelectedDate: [],
        showModal: false,
        currentTask: null
    };

    /* componentDidMount
   * Lifecycle method called when the component is mounted.
   * It fetches the machines and maintenance data from the database.
   */
    componentDidMount() {
        this.fetchMachinesAndWartungen();
    }


    /* This method fetches the list of machines and maintenance tasks from the database using Axios.
     * It updates the component state with the fetched data.
     */
    fetchMachinesAndWartungen = async () => {
        try {
            const machinesResponse = await axios.get('http://localhost:3002/api/maschinen');
            const wartungenResponse = await axios.get('http://localhost:3002/api/wartungen');
            this.setState({ machines: machinesResponse.data }, () => this.filterTasksForSelectedDate(wartungenResponse.data));
        } catch (error) {
            console.error('Error fetching machines or wartungen:', error);
        }
    };

    /* This method is called when a machine is selected from the sidebar.
      * It filters the tasks for the selected machine and updates the state.
      */
    handleFilterByMachine = (machine: string) => {
        this.setState({ selectedMachine: machine }, this.fetchTasksForSelectedDate);
    };

    //This method resets the machine filter and fetches tasks for all machines.

    resetFilter = () => {
        this.setState({ selectedMachine: '' }, this.fetchTasksForSelectedDate);
    };
    /* This method is called when the date is changed in the calendar.
     * It updates the selected date in the state and fetches tasks for the new date.
     */
    handleDateChange: CalendarProps['onChange'] = (value) => {
        if (Array.isArray(value)) {
            // Handle range selection if needed
        } else if (value) {
            this.setState({ selectedDate: value }, this.fetchTasksForSelectedDate);
        }
    };

    /* fetchTasksForSelectedDate
     * This method fetches the maintenance tasks for the selected date from the database.
     */
    fetchTasksForSelectedDate = async () => {
        try {
            const wartungenResponse = await axios.get('http://localhost:3002/api/wartungen');
            this.filterTasksForSelectedDate(wartungenResponse.data);
        } catch (error) {
            console.error('Error fetching wartungen:', error);
        }
    };

    /* filterTasksForSelectedDate
     * This method filters the maintenance tasks based on the selected date and machine.
     * It updates the state with the filtered tasks.
     */
    // Filters tasks based on the selected date and machine
// Filters tasks based on the selected date and machine
    filterTasksForSelectedDate = (wartungen: Wartung[]) => {
        const { selectedDate, selectedMachine, machines } = this.state;
        const selectedDateString = selectedDate.toISOString().split('T')[0];

        const selectedMachineId = machines.find(machine => machine.name === selectedMachine)?.id;

        const tasksForSelectedDate = wartungen.filter(wartung => {
            const wartungDate = wartung.customDate ? new Date(wartung.customDate) : undefined;
            const wartungDateString = wartungDate ? wartungDate.toISOString().split('T')[0] : undefined;

            // Check if the task is for the selected date and machine
            const isTaskForSelectedDate = wartungDateString === selectedDateString ||
                (wartung.wartungsintervalle === 'Weekly' && wartungDate && selectedDate && wartungDate.getDay() === selectedDate.getDay());

            return isTaskForSelectedDate &&
                (!selectedMachine || wartung.maschineId === selectedMachineId);
        });

        this.setState({ tasksForSelectedDate });
    };



    /* This method is called when a task is clicked in the ToDo list.
     * It opens the modal to mark the task as completed.
     */
    handleTaskClick = (task: Wartung) => {
        this.setState({ showModal: true, currentTask: task });
    };

    /* This method is called when the modal is closed.
     * If confirmed, it updates the task as completed in the backend database.
     */
    handleCloseModal = async (confirm: boolean) => {
        if (confirm && this.state.currentTask) {
            try {
                // Update the task as completed in the backend
                await axios.put(`http://localhost:3002/api/wartungen/${this.state.currentTask.id}`, {
                    ...this.state.currentTask,
                    isCompleted: true
                });

                this.setState(prevState => ({
                    tasksForSelectedDate: prevState.tasksForSelectedDate.map(task =>
                        task.id === prevState.currentTask!.id ? { ...task, isCompleted: true } : task
                    ),
                    showModal: false,
                    currentTask: null
                }));
            } catch (error) {
                console.error('Error updating task:', error);
            }
        } else {
            this.setState({ showModal: false, currentTask: null });
        }
    };

    /* This method renders the maintenance schedule for the selected date.
     * It displays the tasks in time slots.
     */
    renderMaintenanceSchedule = () => {
        const { tasksForSelectedDate, machines } = this.state;

        const sortedTasks = tasksForSelectedDate.sort((a, b) => {
            const timeA = a.customDate ? new Date(a.customDate).getTime() : 0;
            const timeB = b.customDate ? new Date(b.customDate).getTime() : 0;
            return timeA - timeB;
        });


        const timeSlots = Array.from({ length: 16 }, (_, i) => i + 6).map(hour => {
            const timeLabel = `${hour.toString().padStart(2, '0')}:00`;
            const tasks = sortedTasks.filter(task => {
                const taskHour = task.customDate ? new Date(task.customDate).getHours() : -1;
                return taskHour === hour;
            });

            return (
                <div key={hour} className="time-slot">
                    <div className="time-label">{timeLabel}</div>
                    {tasks.map((task, index) => {
                        const machine = machines.find(m => m.id == task.maschineId);
                        console.log(`Task: ${task.wartungsname}, Machine ID: ${task.maschineId}, Found Machine: ${machine?.name || 'Unknown'}`);
                        return (
                            <div key={index} className={`task ${task.isCompleted ? 'completed' : ''}`}>
                                {task.wartungsname} ({machine ? machine.name : 'Unknown Machine'})
                            </div>
                        );
                    })}
                </div>
            );
        });

        return <div className="schedule-container">{timeSlots}</div>;
    };

    /* render
        * This method renders the entire CalendarPage component.
        * It includes the sidebar, calendar, maintenance schedule, and ToDo list.
        */
    render() {
        const { selectedMachine, machines, selectedDate, showModal } = this.state;
        const filteredMachines = selectedMachine
            ? machines.filter(machine => machine.name === selectedMachine)
            : machines;

        const selectedDateString = selectedDate.toLocaleDateString('de-DE', {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        });

        return (
            // First left container. It contains two buttons and Filter option
            <Container fluid className="main-content">
                <Row>
                    <Col md={3} className="sidebar bg-light">
                        <SidebarMenu resetFilter={this.resetFilter} />
                        <ListGroup>
                            <Row>
                                {filteredMachines.slice(-4).map((machine, index) => (
                                    <Col key={index} xs={6} className="mb-2">
                                        <ListGroup.Item
                                            onClick={() => this.handleFilterByMachine(machine.name)}
                                            className="list-group-item"
                                        >
                                            {machine.name}
                                        </ListGroup.Item>
                                    </Col>
                                ))}
                            </Row>
                        </ListGroup>
                    </Col>
                    <Col md={6} className="schedule">
                        <h5>Plans for {selectedDateString}</h5>
                        <div className="schedule-list">
                            {this.renderMaintenanceSchedule()}
                        </div>
                    </Col>
                    <Col md={3} className="right-sidebar bg-light">
                        <Calendar
                            className="mb-4"
                            onChange={this.handleDateChange}
                            value={this.state.selectedDate}
                        />
                        <Card className="ToDoList">
                            <Card.Body>
                                <Card.Title className="text-center">ToDo List</Card.Title>
                                <ul className="todo-list">
                                    {this.state.tasksForSelectedDate.map((task, index) => (
                                        <li key={index} className="todo-item" onClick={() => this.handleTaskClick(task)}>
                                            <span className={`todo-time ${task.isCompleted ? 'completed' : ''}`}>
                                                {task.isCompleted ? '✔️' : (task.customDate ? new Date(task.customDate).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }) : 'N/A')}
                                            </span>
                                            <span className={`todo-name ${task.isCompleted ? 'completed' : ''}`}>
                                                {task.wartungsname}
                                            </span>
                                        </li>
                                    ))}
                                </ul>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

                <Modal show={showModal} onHide={() => this.handleCloseModal(false)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Task Completion </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure you want to complete this maintenance?</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => this.handleCloseModal(false)}>
                            No
                        </Button>
                        <Button variant="primary" onClick={() => this.handleCloseModal(true)}>
                            Yes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Container>
        );
    }
}

export default CalendarPage;