/**
 * The MaschineDetail component displays the details of a specific machine and its associated maintenance records.
 * It fetches data from an API and uses the MachineInfo and MaintenanceList components to render the information.
 * The component also incorporates Bootstrap styles and Material icons for formatting.
 */


// Import React hooks: FunctionComponent to define a functional component,
// useEffect for side effects, and useState for local state.
import { FunctionComponent, useEffect, useState } from "react";
// Import the Maschine data model.
import Maschine from "../models/maschine";
import MaschineService from "../services/maschine-service";
// Import the useParams hook from react-router-dom to access URL parameters.
import { useParams } from "react-router-dom";
// Import Bootstrap styles for layout and CSS components.
import 'bootstrap/dist/css/bootstrap.min.css';
// Import custom global styles
import '../styles/globals.css';
// Import the Wartung (maintenance) data model.
import Wartung from "../models/wartung";
import axios from 'axios';
// Import the MachineInfo component to display machine details.
import MachineInfo from "../components/MachineInfo";
// Import the MaintenanceList component to display the maintenance list.
import MaintenanceList from "../components/MaintenanceList";

// Define the Params type to type the URL parameters (here, the machine ID).
type Params = { id: string };

// Define the MaschineDetail functional component.
const MaschineDetail: FunctionComponent = () =>{
    
    // Use useParams to get the machine ID from the URL.
    const { id } = useParams<Params>();

    // Declare a local state to store machine details.
    const [Maschine, setMaschine] = useState<Maschine|null>(null);

    // Declare a local state to store the maintenance list.
    const [wartungen, setWartungen] = useState<Wartung[]>([]);

    useEffect(() => {
      const fetchMachines = async () => {
        try {
            // HTTP request to get the maintenance list from the API.
            const response = await axios.get('http://localhost:3002/api/wartungen');

            // Update the local state with the fetched data.
            setWartungen(response.data);
        } catch (error) {

            // Error handling in case of an issue with the request.
            console.error('Error fetching machines:', error);
        }
    };

    // Call the fetchMachines function to get the maintenance data when the component mounts.
    fetchMachines();
  },[])
   
    // useEffect with an empty dependency array to execute the effect only once when the component mounts.
    useEffect(() => {
      
        const materialIcons = document.createElement('link');
        materialIcons.rel = 'stylesheet';

        // Create a <link> element to add Material icons.
        materialIcons.href = 'https://fonts.googleapis.com/icon?family=Material+Icons';

        // Append the <link> element to the document's <head> to load Material icons.
        document.head.appendChild(materialIcons);

        if (id){
            // If an ID is present, fetch the machine details and update the local state.
            MaschineService.getMaschine(id).then(maschine => setMaschine(maschine));  //ici cetait +id
        }

        return () => {
            // Cleanup: remove the <link> element when the component unmounts.
            document.head.removeChild(materialIcons);
          };
          // useEffect to manage the loading of Material icons and fetching machine details,
      // executed whenever the component mounts or the ID changes.
      }, []);

      const isHistory = MaschineService.checkDirectory(Maschine,wartungen);
      

      return (
        <div className="main-content">
            {Maschine ? (
                <div className="row">
                    <div className="col-md-6">
                        <MachineInfo maschine={Maschine} wartungen={wartungen} />
                    </div>
                    <div className="col-md-6">
                    {isHistory && <MaintenanceList wartungen={wartungen.filter(w => w.maschineId == Maschine.id && !w.isCompleted)} />}
                    {!isHistory && <MaintenanceList wartungen={wartungen.filter(w => w.maschineId == Maschine.id)} />}
                    </div>
                </div>
            ) : (
                <h4 className="center">No machines to display!</h4>
            )}
        </div>

      )
}

// Export the component for use in other parts of the application.
export default MaschineDetail;