import React, { useState, useEffect } from 'react';
import MachineCard from '../pages/MachineCard.tsx';
import { Container, Row, Col, Button } from 'react-bootstrap';
import Maschine from '../models/maschine';
import Wartung from '../models/wartung.ts';
import MaschineService from '../services/maschine-service.ts';
import axios from 'axios';

/**
 * MachineList Component
 * This component is used to display a paginated list of machines.
 * It fetches machines and their maintenance status from the server,
 * filters them based on pending maintenance, and allows pagination through the list.
 */
const MachineList: React.FC = () => {
    const [maschinen, setMaschinen] = useState<Maschine[]>([]);
    const [wartungen, setWartungen] = useState<Wartung[]>([]);
    const [currentPage, setCurrentPage] = useState(1);
    const machinesPerPage = 3;

    useEffect(() => {
        // Function to fetch machines and maintenance data from the server
        const fetchMachines = async () => {
            try {
                const response = await axios.get('http://localhost:3002/api/maschinen');
                setMaschinen(response.data);
            } catch (error) {
                console.error('Error fetching machines:', error);
            }
            try {
                const response = await axios.get('http://localhost:3002/api/wartungen');
                setWartungen(response.data);
            } catch (error) {
                console.error('Error fetching maintenances:', error);
            }
        };

        fetchMachines();
    }, []);

    // Filter machines to show only those with pending maintenance
    const filteredmaschinen = MaschineService.getMachinesWithPendingMaintenance(maschinen, wartungen);

    // Calculate pagination details
    const totalPages = Math.ceil(filteredmaschinen.length / machinesPerPage);
    const indexOfLastMachine = currentPage * machinesPerPage;
    const indexOfFirstMachine = indexOfLastMachine - machinesPerPage;
    const currentMachines = filteredmaschinen.slice(indexOfFirstMachine, indexOfLastMachine);

    // Function to go to the next page
    const nextPage = () => {
        if (currentPage < totalPages) setCurrentPage(currentPage + 1);
    };

    // Function to go to the previous page
    const prevPage = () => {
        if (currentPage > 1) setCurrentPage(currentPage - 1);
    };

    return (
        <Container className="d-flex flex-column justify-content-between">
            <Row className="mb-5">
                {currentMachines.map((machine) => (
                    <Col className="mb-0" key={machine.id} sm="4">
                        <MachineCard machine={machine} isMachineList={true}/>
                    </Col>
                ))}
            </Row>
            <div className="d-flex justify-content-center mt-5 ">
                <Button onClick={prevPage} disabled={currentPage === 1} className="mx-2" style={{
                    backgroundColor: '#4a5c66',
                    borderColor: '#4A5C66',
                    color: 'white'
                }}>Previous</Button>
                <Button onClick={nextPage} disabled={currentPage === totalPages} className="mx-2"
                        style={{backgroundColor: '#2E5A66', borderColor: '#4A5C66', color: 'white'}}>Next</Button>
            </div>
        </Container>
    );
};

export default MachineList;
