/**
 * MachineCard Component
 * This component represents a card that displays information about a machine.
 * When used in a machine list, it shows additional information and provides the ability to complete maintenance.
 */

import React, { useEffect, useState } from 'react';
import { Card, Button, Modal } from 'react-bootstrap';
import Maschine from '../models/maschine'; // Ensure this path is correct
import '../styles/globals.css';
import { useNavigate } from 'react-router-dom';
import Wartung from '../models/wartung';
import 'bootstrap/dist/css/bootstrap.min.css';

interface MachineCardProps {
    machine: Maschine;
    isMachineList?: boolean;
}

const MachineCard: React.FC<MachineCardProps> = ({ machine, isMachineList }) => {
    const [wartungen, setWartungen] = useState<Wartung[]>([]);
    const [showModal, setShowModal] = useState(false);

    const handleClose = () => setShowModal(false);
    const handleShow = () => setShowModal(true);

    const handleConfirm = () => {
        completeMaintenance();
        handleClose();
    };

    // Fetch maintenance data
    useEffect(() => {
        fetch('http://localhost:3002/api/wartungen')
            .then(response => response.json())
            .then((data) => {
                setWartungen(data);
            });

        // Load Material Icons
        const materialIcons = document.createElement('link');
        materialIcons.rel = 'stylesheet';
        materialIcons.href = 'https://fonts.googleapis.com/icon?family=Material+Icons';
        document.head.appendChild(materialIcons);

        return () => {
            document.head.removeChild(materialIcons);
        };
    }, []);

    const navigate = useNavigate();

    // Navigate to machine overview
    const goToMaschine = (id: number) => {
        navigate(`/overview/${id}`);
    };

    // Mark maintenance as completed
    const completeMaintenance = async () => {
        const index = wartungen.findIndex(wartung => !wartung.isCompleted && wartung.maschineId === machine.id);
        const wartungToUpdate = { ...wartungen[index], isCompleted: true };

        try {
            const response = await fetch(`http://localhost:3002/api/wartungen/${wartungen[index].id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(wartungToUpdate)
            });

            if (response.ok) {
                setWartungen(prevWartungen => {
                    const newWartungen = [...prevWartungen];
                    newWartungen[index] = wartungToUpdate;
                    return newWartungen;
                });
            } else {
                console.error('Failed to update maintenance on the server');
            }
        } catch (error) {
            console.error('Error updating maintenance:', error);
        }

        navigate(`/history`);
        navigate(`/overview`);
    };

    return (
        <div className="mt-4">
            <Card className="m-0 shadow-sm">
                <div className="text-white p-3" style={{ backgroundColor: '#2E5A66', marginTop: '-2rem', marginLeft: '-2rem', marginRight: '-2rem', borderRadius: '0.25rem 0.25rem 0 0' }}>
                    <h5 className="m-0">{machine.name}</h5>
                </div>
                <Card.Body>
                    <div className="d-flex justify-content-center mb-3">
                        <img src={machine.picture} alt={`${machine.name} Picture`} className="img-fluid" style={{ maxWidth: '100%', height: 'auto' }} />
                    </div>
                    <Card.Text className="text-left" style={{ fontSize: '1rem', lineHeight: '1rem' }}>
                        <ul className="list-group list-group-flush bg-white">
                            {isMachineList && (
                                <>
                                    <li className="list-group-item bg-white text-dark">
                                        <strong>Processor:</strong> {wartungen.find(wartung => !wartung.isCompleted && wartung.maschineId == machine.id)?.bearbeiter}
                                    </li>
                                    <li className="list-group-item bg-white text-dark">
                                        <strong>Next maintenance:</strong> <span>{wartungen.find(wartung => !wartung.isCompleted && wartung.maschineId == machine.id)?.wartungsname}</span>
                                    </li>
                                </>
                            )}
                        </ul>
                    </Card.Text>
                    <div className="d-flex justify-content-end">
                        <Button
                            className="btn-sm btn-light shadow-sm btn-outline-dark"
                            style={{ position: 'absolute', right: '1rem', bottom: '1rem', width: 'auto' }}
                            onClick={() => goToMaschine(machine.id)}
                        >
                            Learn More
                        </Button>
                        {isMachineList && (
                            <Button
                                className="btn-sm shadow-sm btn-light btn-outline-dark"
                                style={{ position: 'absolute', left: '1rem', bottom: '1rem', width: 'auto' }}
                                onClick={handleShow}
                            >
                                Complete Maintenance
                            </Button>
                        )}

                        <Modal show={showModal} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Confirmation</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>Are you sure you want to complete this maintenance?</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cancel
                                </Button>
                                <Button variant="primary" onClick={handleConfirm}>
                                    Confirm
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </div>
                </Card.Body>
            </Card>
        </div>
    );
};

export default MachineCard;
