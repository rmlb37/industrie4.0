/*
 * ShowMaintenance Component
 * This React component fetches and displays a list of maintenance tasks (Wartung) for a specific machine.
 * The component uses Bootstrap for styling and Axios for API requests.
 */

import React, { useState, useEffect } from "react";
import Wartung from "../models/wartung";
import { useNavigate, useParams } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";
import '../styles/globals.css';
import axios from 'axios';
import MaschineService from "../services/maschine-service";
import Maschine from "../models/maschine";


type Params = { id: string };

const ShowMaintenance: React.FC = () => {
    const { id } = useParams<Params>();   // Get the machine ID from URL parameters
    
    const navigate = useNavigate();
    const [localWartungen, setLocalWartungen] = useState<Wartung[]>([]);  // State to hold local maintenance data
    const [maschinen, setMaschinen] = useState<Maschine[]>([]);    // State to hold machines
    const [wartungen, setWartungen] = useState<Wartung[]>([]);     // State to hold all maintenance tasks
    const [showModal, setShowModal] = useState(false);             // State to control the display of the modal
    const [selectedWartungId, setSelectedWartungId] = useState<number | null>(null);  // State to hold the selected maintenance task ID

    useEffect(() => {
        const fetchMachines = async () => {
            try {
                // Fetch machine data from API
                const response = await axios.get('http://localhost:3002/api/maschinen');
                setMaschinen(response.data); // Update state with fetched machine data
            } catch (error) {
                console.error('Error fetching machines:', error);
            }
            try {
                const response = await axios.get('http://localhost:3002/api/wartungen');
                setWartungen(response.data);  // Update state with fetched maintenance data
            } catch (error) {
                console.error('Error fetching machines:', error);
            }
        };

        
        fetchMachines();  // Call the fetch function when the component mounts

    }, []);

    // Find the machine with the matching ID
    const mashine = maschinen.find(maschine => maschine.id.toString() == id)
    let filteredmaschinen = wartungen;
    if (mashine){ 

    // Get the list of maintenance tasks for the specific machine  
    filteredmaschinen = MaschineService.getMaintenanceList(mashine,wartungen);}

    useEffect(() => {
        if(filteredmaschinen)
        setLocalWartungen(filteredmaschinen);  // Update local maintenance tasks state
    }, [filteredmaschinen]);

    // Function to close the modal
    const handleClose = () => setShowModal(false);

    // Function to show the modal and set the selected maintenance task ID
    const handleShow = (wartungId: number) => {
        setSelectedWartungId(wartungId);
        setShowModal(true);
    };

    // Function to navigate to the edit maintenance task page
    const handleEditClick = (maintenanceId: number) => {
        navigate(`/overview/editMaintenance/${maintenanceId}`);
    };

    const handleConfirm = async () => {
        if (selectedWartungId !== null) {
            const index = localWartungen.findIndex(wartung => wartung.id === selectedWartungId);
            if (index !== -1) {
                const wartungToUpdate = { ...localWartungen[index], isCompleted: true };
                try {
                    const response = await fetch(`http://localhost:3002/api/wartungen/${localWartungen[index].id}`, {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(wartungToUpdate)
                    });

                    if (response.ok) {
                        setLocalWartungen(prevWartungen => {
                            const newWartungen = [...prevWartungen];
                            newWartungen[index] = wartungToUpdate;
                            return newWartungen;
                        });
                    } else {
                        console.error('Failed to update wartung on the server');
                    }
                } catch (error) {
                    console.error('Error updating wartung:', error);
                }
            }
        }
        handleClose();
        navigate(`/history`);
        navigate(`/overview`);
    };

    const handlePdfDownload = (pdfPath: string | null) => {
        const link = document.createElement('a');
        link.href = `http://localhost:3002${pdfPath}`;
        link.setAttribute('download', 'maintenance.pdf');
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    return (
        <div className="">
            <h4 className="head center rounded">Completed maintenance list:</h4>
            {localWartungen.map(wartung => (
                <div key={wartung.id} className="">
                    <div className="card hover-shadow mb-4 position-relative">
                        {!wartung.isCompleted && (
                            <button
                                onClick={() => handleEditClick(wartung.id)}
                                className="btn-floating position-absolute"
                                style={{ top: '10px', right: '10px', border: 'none' }}
                            >
                                <i className="material-icons">edit</i>
                            </button>
                        )}
                        <div className="card-body">
                            <h4 className="mb-4">{wartung.wartungsname}</h4>
                            <table className="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <td>Processor:</td>
                                    <td>{wartung.bearbeiter}</td>
                                </tr>
                                <tr>
                                    <td>Interval:</td>
                                    <td>{wartung.wartungsintervalle}</td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td>{wartung.customDate}</td>
                                </tr>
                                <tr>
                                    <td>Maintenance steps:</td>
                                    <td>
                                        <ul>
                                            {wartung.wartungsschritte.map((schritt, index) => (
                                                <li key={index}>{schritt}</li>
                                            ))}
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Consumables:</td>
                                    <td>
                                        <ul>
                                            {wartung.verbrauchsmaterialien.map((material, index) => (
                                                <li key={index}>{material.name} Menge: {material.quantity}</li>
                                            ))}
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Processing status</td>
                                    <td>{wartung.isCompleted ? 'Abgeschlossen' : 'Offen'}</td>
                                </tr>
                                {wartung.pdf && (
                                    <tr>
                                        <td>PDF</td>
                                        <td>
                                            <Button
                                                variant="outline-primary"
                                                size="sm"
                                                onClick={() => handlePdfDownload(wartung.pdf)}
                                            >
                                                Download PDF
                                            </Button>
                                        </td>
                                    </tr>
                                )}
                                </tbody>
                            </table>
                            {!wartung.isCompleted && (
                                <Button
                                    className="btn-sm shadow-sm btn-light btn-outline-dark"
                                    style={{ position: 'absolute', left: '1rem', bottom: '1rem', width: 'auto' }}
                                    onClick={() => handleShow(wartung.id)}
                                >
                                    Complete maintenance
                                </Button>
                            )}
                        </div>
                    </div>
                </div>
            ))}

            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Bestätigung</Modal.Title>
                </Modal.Header>
                <Modal.Body>Wollen Sie die Wartung wirklich abschließen?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Abbrechen
                    </Button>
                    <Button variant="primary" onClick={handleConfirm}>
                        Bestätigen
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default ShowMaintenance;