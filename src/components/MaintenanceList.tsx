import React, { useState, useEffect } from "react";
import Wartung from "../models/wartung";
import { useNavigate } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";
import '../styles/globals.css';

interface MaintenanceListProps {
    wartungen: Wartung[];
}

const MaintenanceList: React.FC<MaintenanceListProps> = ({ wartungen }) => {
    const navigate = useNavigate();
    const [localWartungen, setLocalWartungen] = useState<Wartung[]>([]);
    const [showModal, setShowModal] = useState(false);
    const [selectedWartungId, setSelectedWartungId] = useState<number | null>(null);

    useEffect(() => {
        setLocalWartungen(wartungen);
    }, [wartungen]);

    const handleClose = () => setShowModal(false);
    const handleShow = (wartungId: number) => {
        setSelectedWartungId(wartungId);
        setShowModal(true);
    };

    const handleEditClick = (maintenanceId: number) => {
        navigate(`/overview/editMaintenance/${maintenanceId}`);
    };

    const handleConfirm = async () => {
        if (selectedWartungId !== null) {
            const index = localWartungen.findIndex(wartung => wartung.id === selectedWartungId);
            if (index !== -1) {
                const wartungToUpdate = { ...localWartungen[index], isCompleted: true };
                try {
                    const response = await fetch(`http://localhost:3002/api/wartungen/${localWartungen[index].id}`, {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(wartungToUpdate)
                    });

                    if (response.ok) {
                        setLocalWartungen(prevWartungen => {
                            const newWartungen = [...prevWartungen];
                            newWartungen[index] = wartungToUpdate;
                            return newWartungen;
                        });
                    } else {
                        console.error('Failed to update maintenance on the server');
                    }
                } catch (error) {
                    console.error('Error updating maintenance:', error);
                }
            }
        }
        handleClose();
        navigate(`/history`);
        navigate(`/overview`);
    };

    const handlePdfDownload = (pdfPath: string | null) => {
        if (pdfPath) {
            const link = document.createElement('a');
            link.href = `http://localhost:3002${pdfPath}`;
            link.setAttribute('download', 'maintenance.pdf');
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    };

    return (
        <div className="">
            <h4 className="head center rounded">Maintenance List:</h4>
            {localWartungen.map(wartung => (
                <div key={wartung.id} className="">
                    <div className="card hover-shadow mb-4 position-relative">
                        {!wartung.isCompleted && (
                            <button
                                onClick={() => handleEditClick(wartung.id)}
                                className="btn-floating position-absolute"
                                style={{ top: '10px', right: '10px', border: 'none' }}
                            >
                                <i className="material-icons">edit</i>
                            </button>
                        )}
                        <div className="card-body">
                            <h4 className="mb-4">{wartung.wartungsname}</h4>
                            <table className="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <td>Handler:</td>
                                    <td>{wartung.bearbeiter}</td>
                                </tr>
                                <tr>
                                    <td>Interval:</td>
                                    <td>{wartung.wartungsintervalle}</td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td>
                                        {wartung.customDate ? (
                                            <>
                                                {new Date(wartung.customDate).toLocaleDateString('de-DE', {
                                                    weekday: 'long',
                                                    year: 'numeric',
                                                    month: 'long',
                                                    day: 'numeric'
                                                })} {' '}
                                                Time: {new Date(wartung.customDate).toLocaleTimeString('de-DE', {
                                                hour: '2-digit',
                                                minute: '2-digit'
                                            })}
                                            </>
                                        ) : (
                                            "No date available"
                                        )}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Maintenance Steps:</td>
                                    <td>
                                        <ul>
                                            {wartung.wartungsschritte.map((schritt, index) => (
                                                <li key={index}>{schritt}</li>
                                            ))}
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Consumables:</td>
                                    <td>
                                        <ul>
                                            {wartung.verbrauchsmaterialien.map((material, index) => (
                                                <li key={index}>{material.name} Quantity: {material.quantity}</li>
                                            ))}
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>{wartung.isCompleted ? 'Completed' : 'Open'}</td>
                                </tr>
                                {wartung.pdf && (
                                    <tr>
                                        <td>PDF</td>
                                        <td>
                                            <Button
                                                variant="outline-primary"
                                                size="sm"
                                                onClick={() => handlePdfDownload(wartung.pdf)}
                                            >
                                                Download PDF
                                            </Button>
                                        </td>
                                    </tr>
                                )}
                                </tbody>
                            </table>
                            {!wartung.isCompleted && (
                                <Button
                                    className="btn-sm shadow-sm btn-light btn-outline-dark"
                                    style={{position: 'absolute', left: '1rem', bottom: '1rem', width: 'auto'}}
                                    onClick={() => handleShow(wartung.id)}
                                >
                                    Complete Maintenance
                                </Button>
                            )}
                        </div>
                    </div>
                </div>
            ))}

            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure you want to complete this maintenance?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={handleConfirm}>
                        Confirm
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default MaintenanceList;
