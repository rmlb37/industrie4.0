/**
 * AddMaintenance Component
 * This component is used to add a new maintenance record for a selected machine.
 * It provides a form where the user can input the machine name, maintenance details, materials, and other relevant information.
 * After input, the data is validated and sent to the server to save the maintenance record in the database.
 */

import React, { useState, useEffect, ChangeEvent } from 'react';
import { Button, Row, Col, Form, InputGroup, Container, Card } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import axios from 'axios';
import '../styles/globals.css';
import { de } from 'date-fns/locale';
import { useNavigate } from 'react-router-dom';
import { CalendarMonth as CalendarMonthIcon, FileUpload as FileUploadIcon } from '@mui/icons-material';

interface Machine {
    id: number;
    name: string;
}

interface User {
    username: string;
    email: string;
}

const AddMaintenance: React.FC = () => {
    // State variables to manage form inputs and messages
    const [machines, setMachines] = useState<Machine[]>([]);
    const [users, setUsers] = useState<User[]>([]);
    const [selectedMachine, setSelectedMachine] = useState<string>('');
    const [selectedUser, setSelectedUser] = useState<string>('');
    const [step, setStep] = useState<string>('');
    const [steps, setSteps] = useState<string[]>([]);
    const [maintenanceName, setMaintenanceName] = useState<string>('');
    const [material, setMaterial] = useState<string>('');
    const [quantity, setQuantity] = useState<string>('');
    const [materials, setMaterials] = useState<{ name: string; quantity: string }[]>([]);
    const [interval, setInterval] = useState<string>('');
    const [customDate, setCustomDate] = useState<Date | null>(null);
    const [pdf, setPdf] = useState<File | null>(null);
    const [errors, setErrors] = useState<{ [key: string]: string }>({});
    const [validated, setValidated] = useState<boolean>(false);
    const [showConfirmation, setShowConfirmation] = useState<boolean>(false);
    const navigate = useNavigate();

    // Fetch machines and users when the component loads
    useEffect(() => {
        fetchMachines();
        fetchUsers();
    }, []);

    // Fetch machines from the API
    const fetchMachines = async () => {
        try {
            const response = await axios.get('http://localhost:3002/api/maschinen');
            setMachines(response.data);
        } catch (error) {
            console.error('Error fetching machines:', error);
        }
    };

    // Fetch users from the API
    const fetchUsers = async () => {
        try {
            const response = await axios.get('http://localhost:3002/api/users');
            setUsers(response.data);
        } catch (error) {
            console.error('Error fetching users:', error);
        }
    };

    // Handler for input field changes
    const handleInputChange = (e: ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => {
        const { name, value, files } = e.target as HTMLInputElement;
        if (name === 'pdf' && files) {
            setPdf(files[0]);
        } else {
            const setStateValue = {
                selectedMachine: setSelectedMachine,
                selectedUser: setSelectedUser,
                maintenanceName: setMaintenanceName,
                material: setMaterial,
                quantity: setQuantity,
                step: setStep,
                interval: setInterval,
            }[name];

            if (setStateValue) {
                setStateValue(value);
            }
            validateField(name, value);
        }
    };

    // Add a maintenance step
    const handleAddStep = () => {
        if (step.trim()) {
            setSteps([...steps, step]);
            setStep('');
        }
    };

    // Change the date
    const handleDateChange = (date: Date | null) => {
        setCustomDate(date);
        validateField('customDate', date);
    };

    // Add a material
    const handleAddMaterial = () => {
        if (material.trim() && quantity.trim()) {
            setMaterials([...materials, { name: material, quantity }]);
            setMaterial('');
            setQuantity('');
        }
    };

    // Validate a field
    const validateField = (fieldName: string, value: string | Date | null) => {
        const newErrors = { ...errors };

        switch (fieldName) {
            case 'selectedMachine':
                newErrors.selectedMachine = !value ? 'Maschine auswählen erforderlich' : '';
                break;
            case 'selectedUser':
                newErrors.selectedUser = !value ? 'Bearbeiter auswählen erforderlich' : '';
                break;
            case 'maintenanceName':
                newErrors.maintenanceName = !value ? 'Wartungsname erforderlich' : '';
                break;
            case 'interval':
                newErrors.interval = !value ? 'Intervall erforderlich' : '';
                break;
            case 'customDate':
                newErrors.customDate = !value ? 'Datum erforderlich' : '';
                break;
            default:
                break;
        }

        setErrors(newErrors);
    };

    // Validate the form
    const validateForm = () => {
        const fieldsToValidate = {
            selectedMachine,
            selectedUser,
            maintenanceName,
            interval,
            customDate: customDate ? customDate : '',
        };

        Object.entries(fieldsToValidate).forEach(([key, value]) => validateField(key, value));

        return Object.values(fieldsToValidate).every(value => value);
    };

    // Save the maintenance record
    const handleSave = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (validateForm() && form.checkValidity()) {
            const formData = new FormData();
            formData.append('selectedMachine', selectedMachine);
            formData.append('selectedUser', selectedUser);
            formData.append('maintenanceName', maintenanceName);
            formData.append('interval', interval);
            if (customDate) {
                formData.append('customDate', customDate.toISOString());
            }
            formData.append('materials', JSON.stringify(materials));
            formData.append('steps', JSON.stringify(steps));
            if (pdf) {
                formData.append('file', pdf);
            }

            try {
                let uploadResponse;
                if (pdf) {
                    uploadResponse = await axios.post('http://localhost:3002/upload', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        },
                    });
                }

                const maintenanceData = {
                    maschineId: selectedMachine,
                    bearbeiter: selectedUser,
                    wartungsname: maintenanceName,
                    verbrauchsmaterialien: materials,
                    wartungsintervalle: interval,
                    wartungsschritte: steps,
                    customDate: customDate ? customDate.toISOString() : null,
                    pdf: uploadResponse ? uploadResponse.data.fileUrl : null,
                    isCompleted: false,
                };

                await axios.post('http://localhost:3002/api/wartungen', maintenanceData);
                setShowConfirmation(true);
                setTimeout(() => {
                    setShowConfirmation(false);
                    navigate('/overview');
                }, 2000);
            } catch (error) {
                console.error('Error saving maintenance:', error);
            }
        } else {
            setValidated(true);
        }
    };

    const minTime = new Date();
    minTime.setHours(6, 0, 0);
    const maxTime = new Date();
    maxTime.setHours(21, 0, 0);

    return (
        <Container fluid className="add-maintenance-container main-content">
            <Card className="mb-4">
                <Form noValidate validated={validated} onSubmit={handleSave}>
                    <Row className="mb-4">
                        <Col className="mb-4" md={12}>
                            <Form.Group controlId="selectedMachine">
                                <h5 className="mb-4">Select a Machine:</h5>
                                <Form.Control
                                    as="select"
                                    name="selectedMachine"
                                    value={selectedMachine}
                                    onChange={handleInputChange}
                                    isInvalid={!!errors.selectedMachine}
                                    required
                                >
                                    <option value="">Select a machine</option>
                                    {machines.map(machine => (
                                        <option key={machine.id} value={machine.id}>
                                            {machine.name}
                                        </option>
                                    ))}
                                </Form.Control>
                                <Form.Control.Feedback type="invalid">
                                    {errors.selectedMachine}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                        <Col md={6}>
                            <Form.Group controlId="selectedUser">
                                <h5 className="mb-4">Select a Technician:</h5>
                                <Form.Control
                                    as="select"
                                    name="selectedUser"
                                    value={selectedUser}
                                    onChange={handleInputChange}
                                    isInvalid={!!errors.selectedUser}
                                    required
                                >
                                    <option value="">Select a user</option>
                                    {users.map(user => (
                                        <option key={user.username} value={user.username}>
                                            {user.username}
                                        </option>
                                    ))}
                                </Form.Control>
                                <Form.Control.Feedback type="invalid">
                                    {errors.selectedUser}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                        <Col md={6}>
                            <Form.Group controlId="maintenanceName">
                                <h5 className="mb-4">Maintenance Name:</h5>
                                <Form.Control
                                    type="text"
                                    name="maintenanceName"
                                    value={maintenanceName}
                                    onChange={handleInputChange}
                                    isInvalid={!!errors.maintenanceName}
                                    required
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errors.maintenanceName}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row className="mb-3">
                        <Col md={6}>
                            <Card>
                                <h5>Material</h5>
                                <Card.Body>
                                    <Form.Group controlId="material">
                                        <Form.Control
                                            type="text"
                                            name="material"
                                            value={material}
                                            onChange={handleInputChange}
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="quantity" className="mt-3">
                                        <Form.Control
                                            type="text"
                                            name="quantity"
                                            value={quantity}
                                            onChange={handleInputChange}
                                        />
                                    </Form.Group>
                                    <Button className="mt-3" variant="outline-secondary" onClick={handleAddMaterial}>
                                        Add Material
                                    </Button>
                                    <Form.Group controlId="materials" className="mt-3">
                                        <Form.Label className="text-left">Materials:</Form.Label>
                                        <Card>
                                            <ul className="list-group">
                                                {materials.map((m, index) => (
                                                    <li key={index} className="list-group-item">
                                                        {m.name} - {m.quantity}
                                                    </li>
                                                ))}
                                            </ul>
                                        </Card>
                                    </Form.Group>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col md={6}>
                            <Card className="mb-4">
                                <h5>Maintenance Step</h5>
                                <Card.Body>
                                    <Form.Group controlId="step">
                                        <InputGroup>
                                            <Form.Control
                                                type="text"
                                                placeholder="Enter Maintenance Step"
                                                name="step"
                                                value={step}
                                                onChange={handleInputChange}
                                            />
                                            <Button variant="outline-secondary" onClick={handleAddStep}>
                                                Add Step
                                            </Button>
                                        </InputGroup>

                                        <Form.Group controlId="steps" className="mt-3">
                                            <Card>
                                                <ul className="list-group">
                                                    {steps.map((s, index) => (
                                                        <li key={index} className="list-group-item">
                                                            {s}
                                                        </li>
                                                    ))}
                                                </ul>
                                            </Card>
                                        </Form.Group>
                                    </Form.Group>
                                </Card.Body>
                            </Card>

                            <Card>
                                <Form.Group controlId="interval">
                                    <h5>Interval</h5>
                                    <div>
                                        <Form.Check
                                            inline
                                            type="radio"
                                            label="Weekly"
                                            name="interval"
                                            value="Weekly"
                                            checked={interval === 'Weekly'}
                                            onChange={handleInputChange}
                                            isInvalid={!!errors.interval}
                                            required
                                        />
                                        <Form.Check
                                            inline
                                            type="radio"
                                            label="One-Time"
                                            name="interval"
                                            value="One-Time"
                                            checked={interval === 'One-Time'}
                                            onChange={handleInputChange}
                                            isInvalid={!!errors.interval}
                                            required
                                        />
                                        <Button variant="outline-secondary" className="ml-2" onClick={() => setInterval('Custom')}>
                                            <CalendarMonthIcon />
                                        </Button>
                                    </div>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.interval}
                                    </Form.Control.Feedback>
                                    {interval === 'Custom' && (
                                        <>
                                            <DatePicker
                                                selected={customDate}
                                                onChange={handleDateChange}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                minTime={minTime}
                                                maxTime={maxTime}
                                                dateFormat="Pp"
                                                inline
                                                locale={de}
                                            />
                                        </>
                                    )}
                                </Form.Group>
                                {customDate && (
                                    <p className="mt-2">
                                        Selected Date: {customDate.toLocaleDateString('de-DE', {
                                        year: 'numeric',
                                        month: 'long',
                                        day: 'numeric',
                                        hour: '2-digit',
                                        minute: '2-digit',
                                    })}
                                    </p>
                                )}
                            </Card>
                        </Col>
                    </Row>

                    <Card className="mb-4">
                        <Row className="mb-4">
                            <Col md={12}>
                                <Form.Group controlId="pdf">
                                    <h5 className="mb-4">PDF Upload / Instruction (optional)</h5>
                                    <InputGroup>
                                        <Form.Control
                                            type="file"
                                            name="pdf"
                                            onChange={handleInputChange}
                                            accept=".pdf"
                                        />
                                        <InputGroup.Text>
                                            <FileUploadIcon />
                                        </InputGroup.Text>
                                    </InputGroup>
                                    {pdf && <p>{pdf.name}</p>}
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card>
                    <Row className="mb-3 justify-content-end">
                        <Col md="auto">
                            <Button variant="secondary" className="me-2">
                                Cancel
                            </Button>
                        </Col>
                        <Col md="auto">
                            <Button type="submit" variant="primary">
                                Save
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Card>

            {showConfirmation && (
                <Row className="mb-3 justify-content-center">
                    <Col md="auto">
                        <div className="alert alert-success" role="alert">
                            Maintenance saved successfully!
                        </div>
                    </Col>
                </Row>
            )}
        </Container>
    );
};

export default AddMaintenance;
