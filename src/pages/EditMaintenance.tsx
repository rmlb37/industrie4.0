/**
 * EditMaintenance Component
 * This component is used to edit an existing maintenance record.
 * It provides a form where the user can input maintenance details, steps, materials, and other relevant information.
 * The data is fetched from the server and sent back when changes are made.
 */

import { FunctionComponent, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Wartung from "../models/wartung";
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { Modal, Button } from 'react-bootstrap';
import DeleteIcon from '@mui/icons-material/Delete';

type Params = { id: string };

const EditMaintenance: FunctionComponent = () => {
    const { id } = useParams<Params>();
    const navigate = useNavigate();
    const [wartung, setWartung] = useState<Wartung | null>(null);
    const [bearbeiter, setBearbeiter] = useState('');
    const [wartungsname, setWartungsname] = useState('');
    const [wartungsschritte, setWartungsschritte] = useState<string[]>([]);
    const [verbrauchsmaterialien, setVerbrauchsmaterialien] = useState<{ name: string, quantity: string }[]>([]);
    const [wartungsintervalle, setWartungsintervalle] = useState('');
    const [customDate, setCustomDate] = useState<Date | null>(null);
    const [pdf, setPdf] = useState<File | null>(null);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [showStepDeleteModal, setShowStepDeleteModal] = useState(false);
    const [showMaterialDeleteModal, setShowMaterialDeleteModal] = useState(false);
    const [deleteStepIndex, setDeleteStepIndex] = useState<number | null>(null);
    const [deleteMaterialIndex, setDeleteMaterialIndex] = useState<number | null>(null);

    // Fetch maintenance data when the component loads
    useEffect(() => {
        if (id) {
            axios.get(`http://localhost:3002/api/wartungen/${id}`)
                .then(response => {
                    if (response.data) {
                        setWartung(response.data);
                        setBearbeiter(response.data.bearbeiter);
                        setWartungsname(response.data.wartungsname);
                        setWartungsschritte(response.data.wartungsschritte);
                        setVerbrauchsmaterialien(response.data.verbrauchsmaterialien);
                        setWartungsintervalle(response.data.wartungsintervalle);
                        setCustomDate(response.data.customDate ? new Date(response.data.customDate) : null);
                    } else {
                        console.error('Maintenance not found');
                    }
                })
                .catch(error => {
                    console.error('Error fetching maintenance:', error);
                    if (error.response && error.response.status === 404) {
                        console.error('Maintenance not found');
                    }
                });
        }
    }, [id]);

    // Save maintenance record
    const handleSave = () => {
        if (wartung) {
            const updatedWartung = { ...wartung, bearbeiter, wartungsname, wartungsschritte, verbrauchsmaterialien, wartungsintervalle, customDate: customDate ? customDate.toISOString() : null };
            axios.put(`http://localhost:3002/api/wartungen/${id}`, updatedWartung)
                .then(() => {
                    navigate('/overview');
                })
                .catch(error => {
                    console.error('Error saving maintenance:', error);
                });
        }
    };

    // Delete maintenance record
    const handleDelete = () => {
        if (id) {
            axios.delete(`http://localhost:3002/api/wartungen/${id}`)
                .then(() => {
                    navigate('/overview');
                })
                .catch(error => {
                    console.error('Error deleting maintenance:', error);
                });
        }
        setShowDeleteModal(false);
    };

    // Confirm and delete a maintenance step
    const handleStepDeleteConfirm = () => {
        if (deleteStepIndex !== null) {
            setWartungsschritte(wartungsschritte.filter((_, i) => i !== deleteStepIndex));
        }
        setShowStepDeleteModal(false);
    };

    // Confirm and delete a material
    const handleMaterialDeleteConfirm = () => {
        if (deleteMaterialIndex !== null) {
            setVerbrauchsmaterialien(verbrauchsmaterialien.filter((_, i) => i !== deleteMaterialIndex));
        }
        setShowMaterialDeleteModal(false);
    };

    // Change a maintenance step
    const handleWartungsschritteChange = (index: number, value: string) => {
        const newWartungsschritte = [...wartungsschritte];
        newWartungsschritte[index] = value;
        setWartungsschritte(newWartungsschritte);
    };

    // Add a maintenance step
    const handleAddWartungsschritt = () => {
        setWartungsschritte([...wartungsschritte, '']);
    };

    // Remove a maintenance step
    const handleRemoveWartungsschritt = (index: number) => {
        setDeleteStepIndex(index);
        setShowStepDeleteModal(true);
    };

    // Change a material
    const handleVerbrauchsmaterialienChange = (index: number, field: string, value: string) => {
        const newVerbrauchsmaterialien = [...verbrauchsmaterialien];
        newVerbrauchsmaterialien[index] = { ...newVerbrauchsmaterialien[index], [field]: value };
        setVerbrauchsmaterialien(newVerbrauchsmaterialien);
    };

    // Add a material
    const handleAddVerbrauchsmaterial = () => {
        setVerbrauchsmaterialien([...verbrauchsmaterialien, { name: '', quantity: '' }]);
    };

    // Remove a material
    const handleRemoveVerbrauchsmaterial = (index: number) => {
        setDeleteMaterialIndex(index);
        setShowMaterialDeleteModal(true);
    };

    // Change the PDF file
    const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files && e.target.files[0];
        if (file) {
            setPdf(file);
        }
    };

    // Remove the PDF file
    const handleRemovePdf = () => {
        setPdf(null);
    };

    // Show and close confirmation modals
    const handleShowDeleteModal = () => {
        setShowDeleteModal(true);
    };

    const handleCloseDeleteModal = () => {
        setShowDeleteModal(false);
    };

    return (
        <div className="main-content d-flex justify-content-center">
            {wartung ? (
                <div className="container">
                    <h2 className="text-center mb-4">Edit Maintenance {wartung.wartungsname}</h2>
                    <div className="row mb-4">
                        <div className="col-md-6 form-group">
                            <h4>Technician</h4>
                            <input
                                type="text"
                                id="bearbeiter"
                                value={bearbeiter}
                                onChange={(e) => setBearbeiter(e.target.value)}
                                className="form-control"
                            />
                        </div>
                        <div className="col-md-6 form-group">
                            <h4>Maintenance Name</h4>
                            <input
                                type="text"
                                id="wartungsname"
                                value={wartungsname}
                                onChange={(e) => setWartungsname(e.target.value)}
                                className="form-control"
                            />
                        </div>
                    </div>
                    <div className="row mb-4">
                        <div className="col-md-6 card form-group">
                            <h4 className="mb-4">Maintenance Steps</h4>
                            {wartungsschritte.map((step, index) => (
                                <div key={index} className="d-flex mb-2 align-items-center">
                                    <input
                                        type="text"
                                        value={step}
                                        onChange={(e) => handleWartungsschritteChange(index, e.target.value)}
                                        className="form-control me-2"
                                        placeholder="Step"
                                    />
                                    <button
                                        type="button"
                                        onClick={() => handleRemoveWartungsschritt(index)}
                                        className="btn btn-danger"
                                    >
                                        <DeleteIcon />
                                    </button>
                                </div>
                            ))}
                            <button type="button" onClick={handleAddWartungsschritt} className="btn btn-primary">
                                Add Step
                            </button>
                        </div>
                        <div className="col-md-6 card form-group">
                            <h4 className="mb-4">Materials</h4>
                            {verbrauchsmaterialien.map((material, index) => (
                                <div key={index} className="d-flex mb-2 align-items-center">
                                    <input
                                        type="text"
                                        value={material.name}
                                        onChange={(e) => handleVerbrauchsmaterialienChange(index, 'name', e.target.value)}
                                        className="form-control me-2"
                                        placeholder="Name"
                                    />
                                    <input
                                        type="text"
                                        value={material.quantity}
                                        onChange={(e) => handleVerbrauchsmaterialienChange(index, 'quantity', e.target.value)}
                                        className="form-control me-2"
                                        placeholder="Quantity"
                                    />
                                    <button
                                        type="button"
                                        onClick={() => handleRemoveVerbrauchsmaterial(index)}
                                        className="btn btn-danger"
                                    >
                                        <DeleteIcon />
                                    </button>
                                </div>
                            ))}
                            <button type="button" onClick={handleAddVerbrauchsmaterial} className="btn btn-primary">
                                Add Material
                            </button>
                        </div>
                    </div>
                    <div className="row mb-4">
                        <div className="col-md-6 card form-group">
                            <h4>Maintenance Interval</h4>
                            <select
                                value={wartungsintervalle}
                                onChange={(e) => setWartungsintervalle(e.target.value)}
                                className="form-control"
                            >
                                <option value="">Select Interval</option>
                                <option value="daily">Daily</option>
                                <option value="weekly">Weekly</option>
                            </select>
                        </div>
                        <div className="col-md-6 card form-group">
                            <h4>Custom Date</h4>
                            <DatePicker
                                selected={customDate}
                                onChange={(date: Date) => setCustomDate(date)}
                                showTimeSelect
                                dateFormat="Pp"
                                className="form-control"
                            />
                        </div>
                    </div>
                    <div className="card form-group mb-4">
                        <h4>PDF Upload</h4>
                        <input
                            type="file"
                            onChange={handlePdfChange}
                            className="form-control"
                            accept=".pdf"
                        />
                        {pdf && (
                            <div className="mt-2">
                                <button type="button" onClick={handleRemovePdf} className="btn btn-danger">
                                    Remove PDF
                                </button>
                                <p>Current PDF: {pdf.name}</p>
                            </div>
                        )}
                    </div>
                    <div className="d-flex justify-content-between">
                        <button onClick={handleShowDeleteModal} className="btn btn-danger">
                            Delete
                        </button>
                        <button onClick={handleSave} className="btn btn-success">
                            Save
                        </button>
                    </div>
                </div>
            ) : (
                <h4 className="center">Loading maintenance data...</h4>
            )}

            <Modal show={showDeleteModal} onHide={handleCloseDeleteModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure you want to delete this maintenance?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseDeleteModal}>
                        Cancel
                    </Button>
                    <Button variant="danger" onClick={handleDelete}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={showStepDeleteModal} onHide={() => setShowStepDeleteModal(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure you want to delete this maintenance step?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShowStepDeleteModal(false)}>
                        Cancel
                    </Button>
                    <Button variant="danger" onClick={handleStepDeleteConfirm}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={showMaterialDeleteModal} onHide={() => setShowMaterialDeleteModal(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure you want to delete this material?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShowMaterialDeleteModal(false)}>
                        Cancel
                    </Button>
                    <Button variant="danger" onClick={handleMaterialDeleteConfirm}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default EditMaintenance;
