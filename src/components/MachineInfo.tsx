import { FunctionComponent, useState } from "react";
import Maschine from "../models/maschine";
import Wartung from "../models/wartung";
import {  Button, Modal } from 'react-bootstrap';
import { Link, useNavigate, useParams } from "react-router-dom";
import MaschineService from "../services/maschine-service";


interface MachineInfoProps {
    maschine: Maschine;
    wartungen: Wartung[];
}

type Params = { id: string };

const MachineInfo: FunctionComponent<MachineInfoProps> = ({ maschine, wartungen }) => {
    const { id } = useParams<Params>();

    const [showModal, setShowModal] = useState(false);

    const navigate = useNavigate();

    const goToAbgeschlosseneWartungsliste = (id: number) =>{
        navigate(`/overview/maintenanceList/${id}`)
    }

    const handleClose = () => setShowModal(false);
    const handleShow = () => setShowModal(true);
    const handleConfirm = () => {
        fetch(`http://localhost:3002/api/Maschinen/${maschine?.id}`,
          {method: 'DELETE', headers: {'Content-Type': 'application/json'}})
          .then(response => response.json());
          handleClose();
          navigate(`/history`) ;
      };

      const isHistory = MaschineService.checkDirectory(maschine,wartungen);
      
    return (
        <div className="machine-info">
            <h2 className="head center rounded">{maschine.name}</h2>
            <div className="card hover-shadow">
                <div>{id && isHistory && <Link to={`/overview/edit/${id}`} className="btn-floating" style={{ position: 'absolute', right: '30rem' }}><i className="material-icons">edit</i></Link>}
                    {id && !isHistory &&<span className="btn-floating halfway-fab waves-effect waves-light" style={{ position: 'absolute', right: '30rem' }} onClick={handleShow}>
                    <i onClick={handleShow} className="material-icons">delete</i></span>}
                </div>
                <div className="card-image">
                    <img src={maschine.picture} alt={maschine.name} style={{width: '255px'}}/>
                </div>
                <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Bestaetigung</Modal.Title>
                </Modal.Header>
                <Modal.Body>Wollen Sie die Maschine wirklich löschen?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        abbrechen
                    </Button>
                    <Button variant="primary" onClick={handleConfirm}>
                        bestaetigen
                    </Button>
                </Modal.Footer>
            </Modal>
                <div className="card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                <td>Processor:</td>
                                {isHistory && <td>{wartungen.find(wartung => !wartung.isCompleted && wartung.maschineId == maschine.id)?.bearbeiter}</td>}
                                {!isHistory && <td>{maschine.zustaendige}</td>}
                            </tr>
                            {isHistory &&<tr>
                                <td>Next maintenance:</td>
                                <td>{wartungen.find(wartung => !wartung.isCompleted && wartung.maschineId == maschine.id)?.wartungsname}</td>
                            </tr>}
                            <tr>
                                <td>Creation date:</td>
                                <td>{maschine.created}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div> {id && isHistory && (
                    <Button className="btn-sm shadow-sm btn-light btn-outline-dark" 
                            style={{ position: 'absolute', left: '1rem', bottom: '1rem', width: 'auto' }}
                            onClick={() => goToAbgeschlosseneWartungsliste(maschine.id)}>
                        Completed maintenance                    </Button> )}
                    
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MachineInfo;

