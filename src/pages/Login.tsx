/**
 * Login Component
 *
 * This component provides a user interface for logging into the application.
 * It includes form fields for the username and password, and communicates
 * with the backend to authenticate the user. Upon successful login, the user
 * is redirected to the overview page.
 */



import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { Button, Container, Row, Col, Alert } from 'react-bootstrap';
import { TextField, InputAdornment } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import LockIcon from '@mui/icons-material/Lock';
import axios from 'axios';
import '../styles/globals.css';

// useState hook to manage the username, password, and errorMessage states
const Login: React.FC = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();

    // handleLogin function to handle form submission and communicate with the backend
    const handleLogin = async (event: React.FormEvent) => {
        event.preventDefault();

        try {
            const response = await axios.post('http://localhost:3002/api/login', {
                username,
                password,
            });

            if (response.data.success) {
                // Store the username in session storage upon successful login
                sessionStorage.setItem('username', username);
                navigate('/overview');
            } else {
                setErrorMessage(response.data.message || 'login failed');
            }
        } catch (error) {
            setErrorMessage('Login failed');
        }
    };

    return (
        <Container className="mt-0 main-content">
            <Row className="mb-4">
                <Col md={12}>
                    <h2 className="text-left">Login</h2>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col md={6} className="d-flex flex-column align-items-center">
                    <form noValidate autoComplete="off" className="w-100" onSubmit={handleLogin}>
                        <TextField
                            label="Username"
                            variant="outlined"
                            fullWidth
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircleIcon />
                                    </InputAdornment>
                                ),
                            }}
                            className="mb-3"
                        />
                        <TextField
                            label="Password"
                            variant="outlined"
                            fullWidth
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <LockIcon />
                                    </InputAdornment>
                                ),
                            }}
                            className="mb-3"
                        />
                        {errorMessage && (
                            <Alert variant="danger" className="mt-2">
                                {errorMessage}
                            </Alert>
                        )}
                        <div className="d-flex justify-content-center">
                            <Button
                                type="submit"
                                className="mt-3"
                                style={{ backgroundColor: '#2E5A66', borderColor: '#4A5C66', color: 'white' }}
                            >
                                Log in
                            </Button>
                        </div>
                        <div className="d-flex justify-content-center mt-3">
                            <Link to="/register" style={{ textDecoration: 'none' }}>
                                <Button variant="link" style={{ color: '#2E5A66' }}>
                                    Not registerd? Register now.
                                </Button>
                            </Link>
                        </div>
                    </form>
                </Col>
            </Row>
        </Container>
    );
};

export default Login;
