// The History component fetches and displays a paginated list of machines that have no pending maintenance more. 
// It uses axios to make HTTP requests to fetch machine and maintenance data from a local server. 
// The component filters machines that do not require maintenance and implements pagination 
// to display a specified number of machines per page. Bootstrap is used for layout and styling.



// Import React hooks: FunctionComponent to define a functional component,
// useEffect for side effects, and useState for local state.
import React, { useState, useEffect } from 'react';
import MachineCard from '../pages/MachineCard.tsx';
import { Container, Row, Col, Button } from 'react-bootstrap';

// Import the Maschine data model.
import Maschine from '../models/maschine';
import Wartung from '../models/wartung.ts';

// Import the service that handles machine-related operations.
import MaschineService from '../services/maschine-service.ts';
import axios from 'axios';

// Define History component as a functional component
const History: React.FC = () => {

    // Initialize state to hold list of machines
    const [maschinen, setMaschinen] = useState<Maschine[]>([]);

    // Initialize state to hold list of maintenance records
    const [wartungen, setWartungen] = useState<Wartung[]>([]);

    // Initialize state to track current page for pagination
    const [currentPage, setCurrentPage] = useState(1);

    const machinesPerPage = 3;

    // useEffect hook to fetch machines and maintenance data from server on component mount
    useEffect(() => {
        const fetchMachines = async () => {
            try {
                // Fetch machine data from API
                const response = await axios.get('http://localhost:3002/api/maschinen');
                setMaschinen(response.data); // Update state with fetched machine data
            } catch (error) {
                console.error('Error fetching machines:', error);
            }
            try {
                const response = await axios.get('http://localhost:3002/api/wartungen');
                setWartungen(response.data);
            } catch (error) {
                console.error('Error fetching machines:', error);
            }
        };

        // Call the function to fetch data when component mounts
        fetchMachines();

    }, []);

    // Filter machines that have no pending maintenance
    const filteredmaschinen = MaschineService.getMachinesWithNoPendingMaintenance(maschinen,wartungen);


    const totalPages = Math.ceil(filteredmaschinen.length / machinesPerPage);// Calculate total number of pages

    const indexOfLastMachine = currentPage * machinesPerPage;// Calculate the index of the last machine on the current page
    const indexOfFirstMachine = indexOfLastMachine - machinesPerPage; // Calculate the index of the first machine on the current page
    const currentMachines = filteredmaschinen.slice(indexOfFirstMachine, indexOfLastMachine);// Get the machines to be displayed on the current page

    // Function to navigate to the next page
    const nextPage = () => {
        if (currentPage < totalPages) setCurrentPage(currentPage + 1);
    };

    // Function to navigate to the previous page
    const prevPage = () => {
        if (currentPage > 1) setCurrentPage(currentPage - 1);
    };

    return (
        <Container className="d-flex flex-column justify-content-between">
            <Row className="mb-4">
                {currentMachines.map((machine) => (
                    <Col key={machine.id} sm="4">
                        <MachineCard machine={machine} isMachineList={false}/>
                    </Col>
                ))}
            </Row>
            <div className="d-flex justify-content-center my-4">
                <Button onClick={prevPage} disabled={currentPage === 1} className="mx-2" style={{ backgroundColor: '#4a5c66', borderColor: '#4A5C66', color: 'white' }}>Previous</Button>
                <Button onClick={nextPage} disabled={currentPage === totalPages} className="mx-2" style={{ backgroundColor: '#2E5A66', borderColor: '#4A5C66', color: 'white' }}>Next</Button>
            </div>
        </Container>
    );
};

// Export the History component as the default export
export default History;