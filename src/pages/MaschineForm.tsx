/** The MaschinenForm component manages the form for editing a machine's details and its associated maintenance tasks. 
* It includes form validation, handles state management, and sends PUT requests to update machine and maintenance data. 
* The component also features a modal dialog for confirming machine deletion.
*/



import React, { FunctionComponent, useEffect, useState } from 'react'; // Import React and hooks for state and lifecycle management
import Maschine from '../models/maschine'; // Import Maschine model
import Wartung from '../models/wartung'; // Import Wartung model
import axios from 'axios'; // Import axios for making HTTP requests
import { useNavigate } from 'react-router-dom'; // Import useNavigate hook for navigation
import { Alert, Form, Button, Modal } from 'react-bootstrap'; // Import Bootstrap components for form, modal, and alerts
import DeleteIcon from '@mui/icons-material/Delete'; // Import DeleteIcon from Material UI
import IconButton from '@mui/material/IconButton'; // Import IconButton from Material UI

type Props = {
    maschine: Maschine
};

type Field = {
    value?: any,
    error?: string,
    isValid?: boolean
}

type Form = {
    zustaendige: Field,
    name: Field,
    naechste_Wartung: Field,
    erstellungsdatum: Field,
    wartungsschritte: Field
}

// Define the MaschinenForm component
const MaschinenForm: FunctionComponent<Props> = ({ maschine }) => {
    const navigate = useNavigate(); // Initialize useNavigate for navigation
    const [showModal, setShowModal] = useState(false); // State for controlling the modal visibility


    const handleClose = () => setShowModal(false);
    const handleShow = () => setShowModal(true);

    // Function to handle delete confirmation
    const handleConfirm = () => {
        deleteM();
        handleClose();
    };

    const [wartungen, setWartungen] = useState<Wartung[]>([]); // State to hold maintenance data
    const [form, setForm] = useState<Form>({
        zustaendige: { value: maschine.zustaendige, isValid: true },
        name: { value: maschine.name, isValid: true },
        naechste_Wartung: { value: "", isValid: true },
        erstellungsdatum: { value: maschine.created, isValid: true },
        wartungsschritte: { value: [], isValid: true }
    });

    // useEffect hook to fetch maintenance data from the server on component mount
    useEffect(() => {
        const fetchMachines = async () => {
            try {
                const response = await axios.get('http://localhost:3002/api/wartungen');
                setWartungen(response.data);
            } catch (error) {
                console.error('Error fetching machines:', error);
            }
        };
        fetchMachines();
    }, []);

    // useEffect hook to update form state when maintenance data is fetched
    useEffect(() => {
        const wartung = wartungen.find(
            (wartung) => !wartung.isCompleted && wartung.maschineId === maschine.id
        );
        if (wartung) {
            const schritte = wartung.wartungsschritte.map((schritt) => schritt);

            setForm((prevForm) => ({
                ...prevForm,
                zustaendige: { value: wartung.bearbeiter },
                naechste_Wartung: { value: wartung.wartungsname, isValid: true },
                wartungsschritte: { value: schritte, isValid: true },
            }));
        }
    }, [wartungen, maschine.id]);

    // Function to handle form input changes
    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const fieldName: string = e.target.name;
        const fieldValue: string = e.target.value;
        const newField: Field = { [fieldName]: { value: fieldValue } };

        setForm({ ...form, ...newField });
    }

    // Function to handle form submission
    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        const wartung = wartungen.find(wartung => !wartung.isCompleted && wartung.maschineId === maschine.id);
        e.preventDefault();
        const isFormValid = validateForm();
        if (isFormValid) {
            maschine.name = form.name.value;
            maschine.created = form.erstellungsdatum.value;
            wartung ? wartung.wartungsname = form.naechste_Wartung.value : "";
            wartung ? wartung.wartungsschritte = form.wartungsschritte.value : "";
            wartung ? wartung.bearbeiter = form.zustaendige.value : "   ";

            fetch(`http://localhost:3002/api/Maschinen/${maschine.id}`,
                { method: 'PUT', body: JSON.stringify(maschine), headers: { 'Content-Type': 'application/json' } })
                .then(response => response.json())

            fetch(`http://localhost:3002/api/Wartungen/${wartung?.id}`,
                { method: 'PUT', body: JSON.stringify(wartung), headers: { 'Content-Type': 'application/json' } })
                .then(response => response.json());
            navigate(`/overview`);
        }
    }

    // Function to validate form fields
    const validateForm = () => {
        let newForm: Form = form;

        // Validator next maintenance
        if (!/^[a-zA-ZàéèäöüÄÖÜ ]{3,50}$/.test(form.naechste_Wartung.value)) {
            const errorMsg: string = 'Der Text enthält ungültige Zeichen oder hat nicht die richtige Länge (3-50).';
            const newField: Field = { value: form.naechste_Wartung.value, error: errorMsg, isValid: false };
            newForm = { ...newForm, ...{ naechste_Wartung: newField } };
        } else {
            const newField: Field = { value: form.naechste_Wartung.value, error: '', isValid: true };
            newForm = { ...newForm, ...{ naechste_Wartung: newField } };
        }
        // Validator name
        if (!/^[a-zA-ZàéèäöüÄÖÜ ]{3,25}$/.test(form.name.value)) {
            const errorMsg: string = 'Der Text enthält ungültige Zeichen oder hat nicht die richtige Länge.(3-25)';
            const newField: Field = { value: form.name.value, error: errorMsg, isValid: false };
            newForm = { ...newForm, ...{ name: newField } };
        } else {
            const newField: Field = { value: form.name.value, error: '', isValid: true };
            newForm = { ...newForm, ...{ name: newField } };
        }
        // Validator bearbeiter
        if (!/^[a-zA-ZàéèäöüÄÖÜ ]{3,25}$/.test(form.zustaendige.value)) {
            const errorMsg: string = 'Der Text enthält ungültige Zeichen oder hat nicht die richtige Länge (3-25).';
            const newField: Field = { value: form.zustaendige.value, error: errorMsg, isValid: false };
            newForm = { ...newForm, ...{ zustaendige: newField } };
        } else {
            const newField: Field = { value: form.zustaendige.value, error: '', isValid: true };
            newForm = { ...newForm, ...{ zustaendige: newField } };
        }
        // Validator Datum
        if (!/^\d{4}-\d{2}-\d{2}$/.test(form.erstellungsdatum.value)) {
            const errorMsg: string = 'Das Datum ist ungültig. Bitte geben Sie ein Datum im Format JJJJ-MM-TT ein.';
            const newField: Field = { value: form.erstellungsdatum.value, error: errorMsg, isValid: false };
            newForm = { ...newForm, ...{ erstellungsdatum: newField } };
        } else {
            const newField: Field = { value: form.erstellungsdatum.value, error: '', isValid: true };
            newForm = { ...newForm, ...{ erstellungsdatum: newField } };
        }

        setForm(newForm);
        return newForm.name.isValid && newForm.zustaendige.isValid && newForm.erstellungsdatum.isValid && newForm.naechste_Wartung.isValid;
    }

    // Function to delete a machine
    const deleteMaschine = () => {
        return fetch(`http://localhost:3002/api/Maschinen/${maschine.id}`,
            { method: 'DELETE', headers: { 'Content-Type': 'application/json' } })
            .then(response => response.json());
    }

    // Function to delete machine and navigate to overview
    const deleteM = () => {
        deleteMaschine();
        navigate(`/overview`);
    }

    return (
        <form onSubmit={e => handleSubmit(e)} >
            <div className="row">
                <div className="col s12 m8 offset-m2">
                    <div className="card hover-shadow">
                        <div className="card-image">
                            <img src={maschine.picture} alt={maschine.name} style={{ width: '250px', margin: '0 auto' }} />
                        </div>
                        <span className="btn-floating halfway-fab waves-effect waves-light d-flex justify-content-center align-items-center bg-danger" style={{ position: 'absolute', right: '18rem', width: '3rem', height: '3rem', borderRadius: '50%' }} onClick={handleShow}>
                            <IconButton aria-label="delete" style={{ color: 'white' }}>
                                <DeleteIcon />
                            </IconButton>
                        </span>
                        <Modal show={showModal} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Bestaetigung</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>Wollen Sie die Maschine wirklich löschen?</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    abbrechen
                                </Button>
                                <Button variant="primary" onClick={handleConfirm}>
                                    bestaetigen
                                </Button>
                            </Modal.Footer>
                        </Modal>
                        <div className="card-stacked" style={{ marginTop: '50px' }}>
                            <div className="card-content">
                                {/* Maschine name */}
                                <div className="form-group">
                                    <label htmlFor="name">Name:</label>
                                    <input id="name" name='name' type="text" className="form-control" value={form.name.value} onChange={e => handleInputChange(e)}></input>
                                    {form.name.error &&
                                        <div className="card-panel red accent-1">
                                            <Alert variant="danger">
                                                {form.name.error} </Alert>
                                        </div>}
                                </div>
                                {/* Bearbeiter */}
                                <div className="form-group">
                                    <label htmlFor="zustaendige">Bearbeiter:</label>
                                    <input id="zustaendige" name='zustaendige' type="text" className="form-control" value={form.zustaendige.value} onChange={e => handleInputChange(e)}></input>
                                    {form.zustaendige.error &&
                                        <div className="card-panel red accent-1">
                                            <Alert variant="danger">
                                                {form.zustaendige.error} </Alert>
                                        </div>}
                                </div>
                                {/* Naechste Wartung */}
                                <div className="form-group">
                                    <label htmlFor="naechste_Wartung">Naechste Wartung:</label>
                                    <input id="naechste_Wartung" name='naechste_Wartung' type="text" className="form-control" value={form.naechste_Wartung.value} onChange={e => handleInputChange(e)}></input>
                                    {form.naechste_Wartung.error &&
                                        <div className="card-panel red accent-1">
                                            <Alert variant="danger">
                                                {form.naechste_Wartung.error} </Alert>
                                        </div>}
                                </div>
                                {/* Datum */}
                                <div className="form-group">
                                    <label htmlFor="erstellungsdatum">Erstellungsdatum:</label>
                                    <input id="erstellungsdatum" name='erstellungsdatum' type="text" className="form-control" value={form.erstellungsdatum.value} onChange={e => handleInputChange(e)}></input>
                                    {form.erstellungsdatum.error &&
                                        <div className="card-panel red accent-1">
                                            <Alert variant="danger">
                                                {form.erstellungsdatum.error} </Alert>
                                        </div>}
                                </div>
                            </div>
                            <div className=" center" style={{ marginTop: '18px' }}>
                                {/* Submit button */}
                                <button type="submit" className=" card-action">Bestätigen</button>
                                {/*<Link to={`/overview`} type="submit" className="card-action " >Bestaetigen</Link>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    )
};

// Export the component for use in other parts of the application.
export default MaschinenForm;