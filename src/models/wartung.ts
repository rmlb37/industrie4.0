/**
 * Wartung Class
 * Represents a maintenance task with its properties.
 */
export default class Wartung {
    id: number;                            // Unique identifier for the maintenance task
    maschineId: number;                    // Identifier for the machine associated with the maintenance task
    bearbeiter: string;                    // Person responsible for the maintenance task
    wartungsname: string;                  // Name of the maintenance task
    verbrauchsmaterialien: { name: string; quantity: string }[]; // List of materials used for the maintenance task
    wartungsintervalle: string;            // Interval at which the maintenance task occurs (e.g., weekly, once)
    wartungsschritte: string[];            // List of steps for the maintenance task
    customDate: string | null;             // Date of the maintenance task as an ISO string or null
    pdf: string | null;                    // Path to the PDF documentation of the maintenance task or null
    isCompleted: boolean;                  // Status indicating if the maintenance task is completed

    /**
     * Constructor to initialize a Wartung object.
     * @param id - Unique identifier for the maintenance task.
     * @param maschineId - Identifier for the machine associated with the maintenance task.
     * @param bearbeiter - Person responsible for the maintenance task.
     * @param wartungsname - Name of the maintenance task.
     * @param verbrauchsmaterialien - List of materials used for the maintenance task.
     * @param wartungsintervalle - Interval at which the maintenance task occurs.
     * @param wartungsschritte - List of steps for the maintenance task.
     * @param customDate - Date of the maintenance task as an ISO string or null.
     * @param pdf - Path to the PDF documentation of the maintenance task or null.
     * @param isCompleted - Status indicating if the maintenance task is completed.
     */
    constructor(
        id: number,
        maschineId: number,
        bearbeiter: string,
        wartungsname: string,
        verbrauchsmaterialien: { name: string; quantity: string }[],
        wartungsintervalle: string,
        wartungsschritte: string[],
        customDate: string | null,
        pdf: string | null,
        isCompleted: boolean
    ) {
        this.id = id;
        this.maschineId = maschineId;
        this.bearbeiter = bearbeiter;
        this.wartungsname = wartungsname;
        this.verbrauchsmaterialien = verbrauchsmaterialien;
        this.wartungsintervalle = wartungsintervalle;
        this.wartungsschritte = wartungsschritte;
        this.customDate = customDate;
        this.pdf = pdf;
        this.isCompleted = isCompleted;
    }
}
