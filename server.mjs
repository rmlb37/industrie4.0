/**
 * This script sets up an Express server with routes for user registration, login, file uploads, and maintenance reminders.
 * It uses various modules to handle different functionalities such as file uploads (multer), email sending (nodemailer), password hashing (bcrypt), and task scheduling (node-cron).
 * The server also provides a JSON-based REST API using json-server.
 * Key modules and their functionality:
 * - jsonServer: Allows creating a REST API based on a JSON file.
 * - express: A web framework for Node.js, simplifies creating web applications and APIs.
 * - bcrypt: Used for hashing passwords.
 * - node-cron: Enables scheduling tasks at specific times.
 * - cors: Enables Cross-Origin Resource Sharing.
 */

import multer from 'multer'; 
import path from 'path'; 
import jsonServer from 'json-server'; 
import express from 'express'; 
import fs from 'fs'; 
import { createTransport } from 'nodemailer';
import { fileURLToPath } from 'url'; 
import { dirname } from 'path'; 
import bcrypt from 'bcrypt'; 
import cron from 'node-cron'; 
import date from 'date-and-time'; 
import cors from 'cors';
import pkg from 'body-parser'; 
const { json } = pkg;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  }
});
const upload = multer({ storage: storage });

// Configuration for email sending
const transporter = createTransport({
  host: 'smtp.ethereal.email',
  port: 587,
  auth: {
    user: 'mckenna.jakubowski@ethereal.email',
    pass: 'gczBYAKyAXKDdjYk24'
  }
});

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const server = express();
const router = jsonServer.router(path.join(__dirname, 'src/models/db.json'));
const middlewares = jsonServer.defaults();
server.use(cors());
server.use(json());

const uploadsDir = path.join(__dirname, 'uploads');
if (!fs.existsSync(uploadsDir)) {
  fs.mkdirSync(uploadsDir);
}

server.use(middlewares);
server.use(express.json());

server.post('/upload', upload.single('file'), (req, res) => {
  if (!req.file) {
    return res.status(400).send('No file uploaded.');
  }

  const filePath = `/uploads/${req.file.filename}`;
  console.log(`File uploaded: ${filePath}`);

  res.json({ fileUrl: filePath });
});

// Function to load the database
const loadDatabase = () => {
  try {
    const data = fs.readFileSync('src/models/db.json', 'utf8');
    const database = JSON.parse(data);
    if (!database.users) {
      database.users = [];
    }
    return database;
  } catch (err) {
    console.error('Error loading the database:', err);
    return { maschinen: [], wartungen: [], users: [] };
  }
};

// Route for user registration
server.post('/api/users', async (req, res) => {
  const { username, email, password } = req.body;
  const database = loadDatabase();
  const existingUser = database.users.find(user => user.username === username);
  if (existingUser) {
    return res.json({ success: false, message: 'Username already taken' });
  }

  const hashedPassword = await bcrypt.hash(password, 10);
  const newUser = { username, email, password: hashedPassword };
  database.users.push(newUser);
  fs.writeFileSync('src/models/db.json', JSON.stringify(database, null, 2));

  res.json({ success: true });
});

// Route for user login
server.post('/api/login', async (req, res) => {
  const { username, password } = req.body;
  const database = loadDatabase();
  sendMaintenanceReminders();

  const user = database.users.find(user => user.username === username);
  if (!user) {
    return res.json({ success: false, message: 'User not found' });
  }

  const isPasswordValid = await bcrypt.compare(password, user.password);
  if (!isPasswordValid) {
    return res.json({ success: false, message: 'Incorrect password' });
  }

  res.json({ success: true, username: user.username });
});

// Function to send maintenance reminders
const sendMaintenanceReminders = () => {
  const database = loadDatabase();
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);

  const reminders = database.wartungen.filter(maintenance => {
    const maintenanceDate = new Date(maintenance.customDate);
    return (
      maintenanceDate.getFullYear() === tomorrow.getFullYear() &&
      maintenanceDate.getMonth() === tomorrow.getMonth() &&
      maintenanceDate.getDate() === tomorrow.getDate()
    );
  });

  const groupedReminders = reminders.reduce((acc, reminder) => {
    if (!acc[reminder.bearbeiter]) {
      acc[reminder.bearbeiter] = [];
    }
    acc[reminder.bearbeiter].push(reminder);
    return acc;
  }, {});
//Groups the worker with their maintenance id's
  Object.keys(groupedReminders).forEach(bearbeiter => {
    const user = database.users.find(user => user.username === bearbeiter);
    if (user) {
      const userReminders = groupedReminders[bearbeiter];
      const emailContent = userReminders.map(reminder => {
        const machine = database.maschinen.find(m => m.id == reminder.maschineId);
        const formattedTime = date.format(new Date(reminder.customDate), 'HH:mm');
        if (machine) {
          return `Machine: ${machine.name} (ID: ${reminder.maschineId})\nTime: ${formattedTime} Uhr\nMaterials: ${reminder.verbrauchsmaterialien.map(item => `${item.name}: ${item.quantity}`).join('\n')} \nMaintenance Steps: ${reminder.wartungsschritte.join(', ')}\n\n`;
        } else {
          return `Unknown Machine (ID: ${reminder.maschineId})\nTime: ${formattedTime} Uhr\nMaterials: ${reminder.verbrauchsmaterialien.map(item => `${item.name}: ${item.quantity}`).join('\n')} \nMaintenance Steps: ${reminder.wartungsschritte.join(', ')}\n\n`;
        }
      }).join('\n');

      const mailOptions = {
        from: 'no-reply@wartungsapp.com',
        to: user.email,
        subject: `Reminder: Upcoming Maintenance on ${tomorrow.toDateString()}`,
        text: `Hello ${bearbeiter},\n\nYou have the following maintenances tomorrow:\n\n${emailContent}\nBest regards,\n The Maintenance App`
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.error('Error sending the email:', error);
        } else {
          console.log('Reminder email sent');
        }
      });
    }
  });
};

// Scheduled task to send maintenance reminders daily at 18:00
cron.schedule('0 18 * * *', () => {
  console.log('Sending maintenance reminders...');
  sendMaintenanceReminders();
});

server.use('/uploads', express.static(uploadsDir));
server.use('/api', router);

// Start the server
server.listen(3002, () => {
  console.log('JSON Server is running on port 3002');
});
